"""
Setup logging module
"""

import logging

console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
console.setFormatter(logging.Formatter('%(levelname)-8s (%(name)s) %(message)s'))

logger = logging.getLogger('')
logger.setLevel(logging.DEBUG)
logger.addHandler(console)

getlog = logging.getLogger
