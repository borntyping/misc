"""
Class traits
"""

# Library imports
import libtcod.libtcodpy as libtcod

class RngTrait ():
	"""	Provides a subclass with a random number generator
		Useful for classes that need a seeded rng.
	"""
	def __init__ (self, seed = None):
		self.rand = libtcod.random_new_from_seed(0xdeadbeef) if seed else libtcod.random_new()

	def random_int (self, a, b):
		return libtcod.random_get_int(self.rand, a, b)
