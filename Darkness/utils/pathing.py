"""
Utility functions for generating paths
"""

# Darkness imports
from log import getlog
log = getlog(__name__)

class StaticPaths ():
	@staticmethod
	def create_path (x1, y1, x2, y2, vertical_first = False):
		"""	Generates a path between two points
			vertical_first: Generate the vertical compontent of the path first
			random: Generate a random path
		"""
		log.debug("Creating a path between {} and {}".format((x1,y1), (x2,y2)))
		v = StaticPaths.create_vertical_path(x1 if vertical_first else x2, y1, y2)
		h = StaticPaths.create_horizontal_path(y1 if not vertical_first else y2, x1, x2)
		return v + h

	@staticmethod
	def create_vertical_path  (x, y1, y2):
		"""	Returns a list of locations in a vertical path """
		log.debug("Creating a vertical path along x:{}, between y:{}-{}".format(x, y1, y2))
		return [(x, y) for y in range(min(y1, y2), max(y1, y2) + 1)]

	@staticmethod
	def create_horizontal_path (y, x1, x2):
		"""	Returns a list of locations in a horizontal path """
		log.debug("Creating a horizontal path along y:{}, between x:{}-{}".format(y, x1, x2))
		return [(x, y) for x in range(min(x1, x2), max(x1, x2) + 1)]
