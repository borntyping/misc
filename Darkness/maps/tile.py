"""
A single map tile
"""

# Library imports
import libtcod.libtcodpy as libtcod

# Can objects be placed in the tile?
TILE_EMPTY			= False	# The tile is empty space
TILE_FILLED			= True	# The tile is filled

class Tile:
	def __init__(self, **kwargs):
		""" Initialise the tile """
		self.set_properties(**kwargs)

		# Default to unexplored
		self.explored = False

		# Default 'filled' to TILE_EMPTY
		if not hasattr(self, 'filled'): self.filled = TILE_EMPTY

	def set_properties (self, **properties):
		"""	Initialise the tile from the given properties """
		self.__dict__.update(properties)

	# Is the tile filled?
	is_filled = property(lambda self: self.filled)

	# Does the tile allow vision to pass though it?
	# Only has effect if the tile is filled
	is_transparent = property(lambda self: hasattr(self, 'transparent'))

	# Can an object move into this tile?
	# - Is the tile filled or otherwise blocking? Then no.
	is_blocking = property(lambda self: self.is_filled or hasattr(self, 'blocking'))

	# Can objects see though this tile?
	# Only if the tile is empty, or both filled and transparent.
	is_blocking_vision = property(lambda self: self.is_filled and not self.is_transparent)
