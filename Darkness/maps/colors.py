"""
Colors
"""

# Library imports
import libtcod.libtcodpy as libtcod

class Colors ():
	class light:
		wall = libtcod.Color(150, 150, 150)
		ground = libtcod.Color(200, 200, 200)

	class dark:
		# Map colors
		wall = libtcod.Color(50, 50, 50)
		ground = libtcod.Color(75, 75, 75)

	class unseen:
		wall = libtcod.Color(0, 0, 0)
		ground = libtcod.Color(1, 1, 1)
