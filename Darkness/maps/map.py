"""
A map of tiles that objects can be placed on
"""

# Library imports
import libtcod.libtcodpy as libtcod
from math import fabs

# Darkness imports
from maps.colors import Colors
from maps.tile import Tile, TILE_EMPTY, TILE_FILLED
from utils.log import getlog
from utils.pathing import StaticPaths
from utils.traits import RngTrait

class Rect ():
	""" A rectangle on the map. used to characterize a room. """
	def __init__ (self, x, y, w, h):
		self.x1, self.y1 = x, y
		self.x2, self.y2 = x + w, y + h

	def __iter__ (self):
		"""	Iterate though the locations *inside* the rectangle """
		for x in xrange(self.x1 + 1, self.x2):
			for y in xrange(self.y1 + 1, self.y2):
				yield x,y

	def __str__ (self):
		"""	Return a string representation of the rectangle """
		return "<Rect ({},{}) ({},{})>".format(self.x1, self.y1, self.x2, self.y2)

	def center(self):
		"""	Returns the center point of the Rect """
		return (int((self.x1 + self.x2) / 2), int((self.y1 + self.y2) / 2))

	def intersect(self, other):
		""" Does this rectangle intersect with the given Rect? """
		return (self.x1 <= other.x2 and self.x2 >= other.x1 and
				self.y1 <= other.y2 and self.y2 >= other.y1)

class Map (RngTrait):
	"""	A map object for a single map of a given width and height """
	def __init__ (self, width, height, seed = None):
		"""	Initialise the map """
		self.width, self.height = width, height
		self.console = libtcod.console_new(width, height)

		# Setup logging and log the type of map being created
		self.log = getlog(str(self.__class__))
		self.log.info("Generating a {}".format(self.__class__.__name__))

		# Setup rng
		RngTrait.__init__(self, seed)

		# Setup tiles and fill with None
		self.tiles = [ [ None for x in range(self.height) ] for y in range(self.width) ]

		# Setup objects list
		self.objects = []

		# Default spawn point
		self.spawn = (1,1)

	def __iter__ (self):
		for x in xrange(self.width):
			for y in xrange(self.height):
				yield x,y

	"""	Initialisation """

	def create_fov_map (self):
		"""	Create the fov map """
		self.fov = libtcod.map_new(self.width, self.height)
		for x, y in self:
			libtcod.map_set_properties(self.fov, x, y, not self.tiles[x][y].filled, self.tiles[x][y].is_filled)

	def fill (self, *args, **kwargs):
		"""	Fill the map with tiles - *args is passed to the Tile constructor """
		for x, y in self:
			self.tiles[x][y] = Tile(*args, **kwargs)

	""" Object/location handling """

	def is_in_bounds (self, x, y):
		"""	Is the given location within bounds? """
		return (self.width > x >= 0) and (self.height > y >= 0)

	def is_blocked (self, x, y):
		"""	Is the given location blocked by a filled tile or an object? """
		return self.tiles[x][y].is_blocking or self.is_blocked_by_object(x,y)

	def is_blocked_by_object (self, x, y):
		"""	Is the given location blocked by an object? If so, return the object. """
		for object in self.objects:
			if not object.has_tag('nonblocking') and object.x == x and object.y == y:
				return object
		return False

	"""	Drawing """

	def draw (self):
		"""	Draw the map """
		for x, y in self:
			self.draw_tile(x, y)

		# Draw objects
		for object in self.objects:
			object.draw()

		# Draw the map onto the display
		libtcod.console_blit(self.console, 0, 0, self.width, self.height, 0, 0, 0)
		libtcod.console_flush()

		# Clear objects
		for object in self.objects:
			object.clear()

	def draw_tile (self, x, y):
		"""	Draw a single tile """
		# If tile is visible:
		if libtcod.map_is_in_fov(self.fov, x, y):
			colors = Colors.light
			# Set tile to explored
			self.tiles[x][y].explored = True
		# If tile is not visible, but explored
		elif self.tiles[x][y].explored:
			colors = Colors.dark
		# If tile is not visible or explored, do not draw it
		else:
			colors = Colors.unseen
		color = colors.wall if self.tiles[x][y].filled else colors.ground
		libtcod.console_set_back(self.console, x, y, color, libtcod.BKGND_SET)
