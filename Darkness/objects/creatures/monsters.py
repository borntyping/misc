"""
Monsters
"""

# Darkness imports
from creature import Creature
from traits import Pushable

class Monster (Creature, Pushable):
	def __init__ (self, *args, **kwargs):
		Creature.__init__(self, *args, **kwargs)

	def think (self):
		"""	Take a turn """
		#print '{} growls!'.format(self.name)
		pass
