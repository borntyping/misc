"""
Player object
"""

# Library imports
import libtcod.libtcodpy as libtcod

# Darkness imports
from creature import Creature, CouldNotMove
from traits import Pushable

class Player (Creature, Pushable):
	def __init__ (self):
		Creature.__init__(self, '@', libtcod.red)

	def move (self, dx = 0, dy = 0):
		"""	Move the player object

			Attempting to move into the same space as another
			object will attempt to push it out of the way """
		x, y = self.x + dx, self.y + dy

		# If there is an object in the location the player
		# attempts to move to, try and push it
		target = self.map.is_blocked_by_object(x,y)
		if target and hasattr(target, 'push'):
			try:
				return target.push(dx, dy)
			except CouldNotMove:
				print "Can't shove the target in this direction."
			except AttributeError:
				print "The target cannot be pushed."

		# Otherwise, move normally
		Creature.move(self, dx, dy)
