"""
Creature - base class for monsters, npcs etc.
"""

# Library imports
import libtcod.libtcodpy as libtcod

# Darkness imports
from objects.object import Object
from objects.attributes import Counter

class CouldNotMove (Exception):
	"""	An object could not be moved """
	def __init__ (self, x, y, reason):
		Exception.__init__(self, reason)
		self.x, self.y = x, y

	def __str__ (self):
		return "The object could not be moved to ({},{}): {}".format(self.x, self.y, self.message)

class Creature (Object):
	def __init__ (self, *args, **kwargs):
		Object.__init__(self, *args, **kwargs)

		# Set default attributes
		self.attributes = {
			'hp': Counter(10),
		}

		# Update the attributes dict with given attributes
		if 'attributes' in kwargs:
			self.attributes.update(kwargs['attributes'])

	def set_attributes (self, hp):
		"""	Set the creatures basic attributes """
		self.hp = hp

	"""	Movement """

	def move (self, dx = 0, dy = 0):
		"""	Move the object, checking that the move is valid first.

			Throws a CouldNotMove exception if the move is invalid,
			else returns True. """
		x, y = self.x + dx, self.y + dy
		if not self.map.is_in_bounds(x,y):
			reason = "You many not move out of bounds."
		elif self.map.is_blocked(x,y):
			reason = "You are blocked from moving there."
		else:
			return self.set_position(x,y)
		raise CouldNotMove(x, y, reason)