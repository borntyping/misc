"""
Traits that can be used as part of creatures
"""

class Pushable ():
	"""	A trait for object that allows them to be pushed """
	def push (self, dx, dy):
		self.move(dx, dy)
