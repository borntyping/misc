"""
Object stats
"""

class Attribute:
	pass

class Counter (Attribute):
	"""	An attribute with a max value and a current value (such as health) """
	def __init__ (self, maximum, value = None):
		self.max = maximum
		self.value = value if value else value

	def __call__ (self):
		"""	Return a tuple with the counters current value and maximum """
		return self.value, self.max

	def modify (self, value, ignore_limit = False):
		"""	Add 'value' to the counters value """
		self.value = value

		# Limit to the counters max value unless
		# 'ignore_limit' is True
		if self.value > self.max and not ignore_limit:
			self.value = self.max