"""
Generic object superclass

Always represented by a character on screen

Tags:
	nonblocking - The object does not block movment of other objects
	pushable - The object can be pushed

"""

# Library imports
import libtcod.libtcodpy as libtcod

class Object:
	def __init__(self, char, color, tags = None, name = None):
		"""	Initialise an object, that can be placed onto a map """
		self.map = None
		self.color = color

		# Assert that 'char' is actually a char
		assert isinstance(char, str) and len(char) == 1, "'char' must be a string of 1 character (recived {!r} instead)".format(char)
		self.char = char

		# Object tags
		self.tags = tags if tags else []

		# Object name, defaults to the class name
		self._name = name if name else self.__class__.__name__

	def place_on_map (self, map, x = None, y = None):
		"""	Place the object on a map, at a given location or the map spawn location. """
		# If the object is already on a map, remove it
		if self.map:
			self.map.objects.remove(self)
		# Place the object on a map
		self.map = map
		self.map.objects.append(self)
		self.set_position(x if x else self.map.spawn[0], y if y else self.map.spawn[1])


	"""	Setters """

	def set_position(self, x, y):
		"""	Set the position of the object """
		self.x, self.y = x, y

	"""	Object tags """

	def has_tag (self, tag):
		return tag in self.tags

	@property
	def name (self):
		"""	Return the objects name """
		return self._name

	"""	Drawing """

	@property
	def visible (self):
		""" Is the item visible to the player? """
		return libtcod.map_is_in_fov(self.map.fov, self.x, self.y) or self.has_tag('always_show')

	def draw(self):
		""" Set the color and then draw the character that represents this object at its position,
			only if the object is in the fov or tagged with 'allways_show' """
		if self.visible:
			libtcod.console_set_foreground_color(self.map.console, self.color)
			libtcod.console_put_char(self.map.console, self.x, self.y, self.char, libtcod.BKGND_NONE)

	def clear(self):
		""" Erase the character that represents this object """
		libtcod.console_put_char(self.map.console, self.x, self.y, ' ', libtcod.BKGND_NONE)