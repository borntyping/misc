"""
Darkness, a sci-fi rougelike using libtcodpy
"""

# libtcod library
import libtcod.libtcodpy as libtcod

# Darkness imports
from maps.colors import Colors
from maps.dungeon import RandomDungeon
from objects.creatures.player import Player, CouldNotMove
from utils.log import getlog

class Darkness ():
	def __init__ (self):
		""" Initialise the game """
		# Setup logging
		self.log = getlog(self.__class__.__name__)

		# Console size
		self.width = 80
		self.height = 50

		# libtcod initialization
		libtcod.console_set_custom_font('consolas10x10_gs_tc.png', libtcod.FONT_TYPE_GREYSCALE | libtcod.FONT_LAYOUT_TCOD)
		libtcod.console_init_root(self.width, self.height, 'Darkness', False)

		# Command initialization
		self.commands = {
			# Commands that do not cause the player to take a turn
			libtcod.KEY_ESCAPE: lambda self: self.stop(),
		}
		self.ingame_commands = {
			# Commands that are a player turn
			libtcod.KEY_UP:		lambda self: self.player.move(dy = -1),
			libtcod.KEY_DOWN:	lambda self: self.player.move(dy = +1),
			libtcod.KEY_LEFT:	lambda self: self.player.move(dx = -1),
			libtcod.KEY_RIGHT:	lambda self: self.player.move(dx = +1),
		}

		# Select options
		self.options = {
			'fov_torch_radius': 6,
			'fov_light_walls': True,
		}

		# Create player
		self.player = Player()

		# Load map
		map_type, map_args = RandomDungeon, ()
		self.load_map(map_type, *map_args)


	""" Initialisation """
	def load_map (self, map_type, *map_args, **map_kwargs):
		# Create a map
		self.map = map_type(self.width, self.height, *map_args, **map_kwargs)
		self.map.create_fov_map()

		# Place player on map
		self.player.place_on_map(self.map)

	"""	Input """

	def input (self):
		""" Handle input

			Returns True if a turn was taken """
		key = libtcod.console_wait_for_keypress(True)

		if key.vk == libtcod.KEY_ENTER and key.lalt:
			return libtcod.console_set_fullscreen(not libtcod.console_is_fullscreen())

		# Try commands
		for k in self.commands:
			if key.vk == k:
				self.commands[k](self)
				return False

		# Try ingame commands
		try:
			for k in self.ingame_commands:
				if key.vk == k:
					self.ingame_commands[k](self)
					return True
		except CouldNotMove as e:
			self.log.debug(e)
			return False

	"""	Drawing """

	def draw (self):
		# Light map
		libtcod.map_compute_fov(
			self.map.fov, self.player.x, self.player.y,
			self.options['fov_torch_radius'], self.options['fov_light_walls'], 1)

		# Draw the map
		self.map.draw()

	""" Main loop """

	def run (self):
		""" Main loop """
		while not libtcod.console_is_window_closed() and not hasattr(self, 'stopped'):
			# Draw
			self.draw()

			# Handle keys
			turn = self.input()

			# If a turn has not been take, continue
			if not turn:
				continue

			# Think
			for object in self.map.objects:
				if hasattr(object, 'think'):
					object.think()

	def stop (self):
		"""	Stop the game """
		self.stopped = True

if __name__ == '__main__':
	Darkness().run()