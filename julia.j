f{A,B}(x::A, y::B, z::A, a::B) = true
f(x,y,z,a) = false

f(1,"string",2,"string") == true
f(1,1,1,1) == true
f(1,1,1,2.0) == false

