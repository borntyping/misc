from grid import Grid
from random import Random

def avg(values): return sum(values,) / len(values)

class Nodegen:
	rng = Random()
	depth = 1

	def __init__(self,sizex,sizey):
		self.grid = Grid(sizex,sizey,2)
		colors = [(0,0,255), (0,255,0), (255,0,0), (255,255,0), (0,255,255), (255,0,255), (255,255,255), (0,0,0)]
		for x,y in self.grid:
			self.grid[(x,y)] = self.rng.choice(colors)#("s", "l", "m"))

	def square(self, topleft, topright, bottomleft, bottomright, origin, default = None):
		square = Grid(3,3)
		square[(0,0)] = topleft
		square[(2,0)] = topright
		square[(0,2)] = bottomleft
		square[(2,2)] = bottomright

		choices = []
		abc = [
			[(1,0),(0,0),(2,0)],
			[(0,1),(0,0),(0,2)],
			[(2,1),(2,0),(2,2)],
			[(1,2),(0,2),(2,2)],
		]
		for a,b,c in abc:
			square[a] = self.rng.choice((square[b], square[c]))
			#square[a] = sum((square[b], square[c])) / 2
			choices.append(square[a])
		square[(1,1)] = self.rng.choice( choices )

		#d = 40/self.depth
		#square[(1,1)] = (sum((topleft, topright, bottomleft, bottomright)) / 4) + self.rng.randint(-d,d)
		#for x,y in square:
		#	square[(x,y)] = int(abs(square(x,y)))

		square.origin = origin
		return square

	def increase(self, grid):
		squares = []
		newgrid = Grid((grid.width * 2) - 1,(grid.height * 2) - 1)
		for x in xrange(grid.width-1):
			for y in xrange(grid.height-1):
				s = self.square(grid(x,y), grid(x+1,y), grid(x,y+1), grid(x+1,y+1), origin = (x,y))
				squares.append(s)
		for square in squares:
			i,j = square.origin
			i,j = 2*i, 2*j
			for x,y in square:
				newgrid[(x+i,y+j)] = square[(x,y)]
		self.grid = newgrid
		return newgrid

	def generate(self, num):
		for i in xrange(1,num+1):
			self.depth = i
			self.grid = self.increase(self.grid)
			print "Cycle %s - size: %s by %s " % (i, self.grid.width, self.grid.height)

	def save(self, filename):
		import pygame
		worldmap = pygame.Surface((self.grid.width, self.grid.height))
		for x, y in self.grid:
			worldmap.set_at( (x, y), self.grid.get(x,y) )
		pygame.image.save(worldmap, filename)

n = Nodegen(5,5)
n.generate(7)
n.save('./worldmap.png')
