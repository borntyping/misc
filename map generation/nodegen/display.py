import pygame

class Display:
	# Create display
	def __init__(self, world):
		self.world = world
		self.DISPLAYSIZE = (self.world.width, self.world.height)
		#pygame.display.init()
		#pygame.display.set_caption('nodegen')
		#self.screen = pygame.display.set_mode(self.DISPLAYSIZE)

	def draw_worldmap(self):
		worldmap = pygame.Surface(self.DISPLAYSIZE)
		for x, y in self.world:
			worldmap.set_at( (x, y), self.world.get(x,y) )
		pygame.image.save(worldmap, './worldmap.png')

	def wait(self):
		while 1:
			event = pygame.event.wait()
			if (event.type == pygame.QUIT):
				exit()
			elif (event.type == pygame.MOUSEBUTTONDOWN):
				return
