class Grid:
	def __init__(self, width, height, default = None):
		self.width, self.height = width, height
		self.grid = []
		for X in range(self.width):
			row = []
			for Y in range(self.height):
				row.append(default)
			self.grid.append(row)

	# Get and set values

	def get(self, x, y):
		if (x > self.width-1) or (y > self.height-1) or (x < 0) or (y < 0):
			raise GridError('Co-ordinates out of bounds.')
		return self.grid[x][y]

	def set(self, x, y, value):
		self.grid[x][y] = value
		return value

	def __getitem__(self, key):
		if isinstance(key, tuple):
			return self.get(key[0], key[1])
		else:
			return self.grid[key]

	def __setitem__(self, key, value):
		return self.set(key[0], key[1], value)

	def __call__(self, x, y, value = None):
		if value == None:
			return self.get(x,y)
		else:
			return self.set(x,y,value)

	# View values

	def __iter__(self):
		for x in range(self.width):
			for y in range(self.height):
				yield x,y

	def __str__(self):
		string = ""
		for x in range(self.width):
			for y in range(self.height):
				string += str(self.get(x,y)) + "\t"
			string += "\n"
		string = string.strip()
		return string

class GridError(Exception):
	pass
