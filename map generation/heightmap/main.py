#!/usr/bin/env python

import pygame
from grid import Grid
from rectangle import Rectangle

class HeightmapGenerator ():
	def __init__(self):
		self.chunks = dict()

	def generate_chunk(self, coords):
		height, width = (5,5)
		grid = Grid((height, width))
		start = Rectangle( (0,0), (height-1, width-1) )
		for point in start:
			print point
			grid[point] = 10
		self.chunks[coords] = grid
	

if __name__ == '__main__':
	hm = HeightmapGenerator()
	hm.generate_chunk((0,0))
	print hm.chunks[0,0].size
	print hm.chunks[0,0]