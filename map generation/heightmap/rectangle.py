class Rectangle (object):
	__slots__ = ('tl','br')#,'t','l','b','r','__iter__')
	def __init__(self, tl, br):
		self.tl, self.br = tl, br
		
	@property
	def t(self): return self.tl[0]
	@property
	def l(self): return self.tl[1]
	@property
	def b(self): return self.br[0]
	@property
	def r(self): return self.br[1]
	
	@property
	def tc(self): return self.t, (self.l + self.r) / 2

	@property
	def size(self): return self.b - self.t + 1, self.r - self.l + 1
	
	def __iter__(self):
		for a in (self.t, self.b):
			for b in (self.l, self.r):
				yield a,b

if __name__ == '__main__':
	r = Rectangle((0,0),(4,4))
	print r
	print r.size