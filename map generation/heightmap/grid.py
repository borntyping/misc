class Grid ():
	def __init__(self, size):
		self.set_size(size)
		x,y = self.get_size()
		self.create_grid()

	# Properties
	
	def get_size(self): return (self.x, self.y)
	def set_size(self, size): self.x, self.y = size
	size = property(get_size, set_size)

	def __getitem__(self, key):
		return self.data[key[0]][key[1]]
	
	def __setitem__(self, key, value):
		self.data[key[0]][key[1]] = value
	
	def __delitem__(self, key):
		self.data[key[0]][key[1]] = None
	
	def __len__(self):
		return self.x * self.y
	
	def __iter__(self):
		"""Iterates over the coordinates for each cell in the grid."""
		for x in range(self.x):
			for y in range(self.y):
				yield x,y
	
	# Logic
	
	def create_grid(self):
		self.data = self._create_grid()
		return self.data
	
	def _create_grid(self):
		X,Y = self.get_size()
		sliceX = []
		for x in xrange(X):
			sliceY = []
			for y in xrange(Y):
				cell = "x%s,y%s" % (x,y)
				#cell = ""
				sliceY.append(cell)
			sliceX.append(sliceY)
		return sliceX
	
	# Printing
	
	def __str__(self):
		return self.string()
	
	def string(self, grid = None, spacer = '\t', newline = '\n'):
		if grid == None:
			grid = self.data
		string = ""
		for x in xrange(self.x):
			if x != 0:
				string += newline
			for y in xrange(self.y):
				string += str(self[x,y])
				string += spacer
		return string

if __name__ == '__main__':
	g = Grid((5,5))
	g[1,1] = 'hello'
	del g[2,2]
	print g