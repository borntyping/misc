def save_world(world):
	from pickle import dump
	f = open("world.pickle","w")
	dump(world, f)
	f.close()

def load_world(biome_func):
	from pickle import load
	f = open("world.pickle","r")
	world = load(f)
	f.close()
	
	world.set_biome_func( biome_func )
	return world
