from grids import GridError

class Particle():
	def __init__(self, world, location, ttl):
		self.world = world
		self.r = self.world.r
		self.location = location
		self.ttl = ttl
		self.create()

	def create(self):
		while (self.ttl > 0):
			self.location = self.fall(self.location)
		self.world[self.location] += 1

	def fall(self, current_location = None):
		if current_location == None:
			current_location = self.location
			
		# Get neighbours
		neighbours = self.get_neighbours( current_location )

		# Pick a random neighbour and use it as the smallest
		nlocation = self.r.choice(neighbours)		
		smallest = self.world[ nlocation ]
		smallest_location = [nlocation,]

		# Find the smallest neighbour
		# If a neighbour is equaly small, add it to the list of coords
		for location in neighbours:
			if self.world[location] < smallest:
				smallest = self.world[ location ]
				smallest_location = [location,]
			elif self.world[location] == smallest:
				smallest_location.append(location)

		# Reduce time to live
		if self.ttl == -1:
			pass
		elif self.r.randint(0,self.ttl) == 0:
			self.ttl == 0
		else:
			self.ttl -= 1

		# Pick one of the coords
		return self.r.choice( smallest_location )

	def get_neighbours(self, location):
		n = list()
		x,y = location
		for i in (-1, 0, 1):
			for j in (-1, 0, 1):
				if (i,j) == (0,0):
					continue
				try:
					self.world(x+i, y+j)
				except GridError:
					pass
				else:
					n.append( (x+i, y+j) )
		return n

class Spring (Particle):
	def create(self):
		nl = self.location
		for i in xrange(25):
			nl = self.fall(nl)
			self.world[nl] = '~'
