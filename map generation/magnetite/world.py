import grids
import random
import colours

from particle import *

WATERLEVEL = 0

class World(grids.Grid):
	def __init__(self, width, height, seed, biome_func):
		self.biome_func = biome_func
		grids.Grid.__init__(self, width, height)
		self.r = random.Random()
		self.r.seed(seed)
	
	def __str__(self):
		"""Return a string representation of self."""
		string = ""
		for y in range(self.height):
			for x in range(self.width):
				string += self.biome_func( self.__getitem__((x,y)) )
			string += colours.normal + "\n"
		string = string.strip()
		return string

	def get_random_location(self):
		return ( self.r.randint(0, self.width-1), self.r.randint(0, self.height-1) )

	def generate(self, particles, life):
		"""	Start with a blank map.
			Pick a random starting spot for the particle.
			Increment the value on the map by 1 where the particle is located.
			Move the particle to a random adjacent position whose value is less than or equal to the current position (imagine the particle rolling downhill).
			Repeat the last 2 steps as long as the particle is alive.
			Repeat for a large number of particles.
		"""
		for x, y in self:
			self(x,y,0)

		a = b = 0
		for i in xrange(particles):	
			if a == int( particles / 10 ) - 1:
				a, b = 0, b + 10
				print "%s of %s particles [%s%%]" % (i + 1, particles, b)
			else:
				a += 1

			#particle_location = self.get_random_location()
			#particle_location = self.height / 2 , self.width / 2
			pad = 20
			particle_location = self.r.randint(pad, self.width - pad), self.r.randint(pad, self.height - pad )
			Particle(self, particle_location, life)

	def set_biome_func(self, biome_func):
		self.biome_func = biome_func
