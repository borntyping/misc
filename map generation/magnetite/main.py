#!/usr/bin/env python

from sys import argv

import colours

from world import *
from functions import *

def biomes(s):
	t = str(s)
	if s == '~':
		return colours.blue + t
	if s > 4:
		return colours.white + t
	elif s > 2:
		return colours.green + t
	elif s > WATERLEVEL:
		return colours.bright.green + t
	return colours.blue + '~'

if len(argv) > 1 and argv[1] == "--load":
	world = load_world(biomes)
	springs = [(30,25), (50,15), (75,25), (50,25)]
	for s in springs:
		world[s] = Spring(world, s, -1)
	print world
else:
	world = World(100, 50, "SEED", biomes)
	world.generate(10000, 25)
	print world
	save_world(world)
