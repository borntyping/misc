"""	Grid data object """

class GridError(Exception):
	pass

class Grid:
	def __init__(self, width, height):
		"""Create a grid of proportions 'width' and 'height'."""
		self.size = self.width, self.height = width, height
		self.grid = []
		for x in range(self.width):
			col = []
			for y in range(self.height):
				col.append(None)
			self.grid.append(col)

	#~ def get(self, x, y):
		#~ return self.__getitem__((x,y))
	#~ def set(self, x, y, value):
		#~ return self.set((x,y), value)
	#~ def delete(self, x, y):
		#~ self.__delitem__((x,y))

	def __getitem__(self, key):
		"""Get a grid item."""
		x,y = key
		if (x > self.width-1) or (y > self.height-1) or (x < 0) or (y < 0):
			raise GridError('Co-ordinates out of bounds.')
		return self.grid[x][y]

	def __setitem__(self, key, value):
		"""Set a grid item."""
		self.grid[key[0]][key[1]] = value
		return value

	def __delitem__(self, key):
		"""Delete a grid item."""
		self.grid[key[0]][key[1]] = None

	def __call__(self, x, y, value = None):
		"""Get or set a grid value when self is called."""
		if value == None:
			return self.__getitem__((x,y))
		else:
			return self.__setitem__((x,y),value)

	def __iter__(self):
		"""Used when self is iterated over in a for loop."""
		for y in range(self.height):
			for x in range(self.width):
				yield x,y

	def __str__(self):
		"""Return a string representation of self."""
		string = ""
		for y in range(self.height):
			for x in range(self.width):
				s = self.__getitem__((x,y))
				if s == None: s = "-"
				string += str(s).ljust(10)
			string += "\n"
		string = string.strip()
		return string
