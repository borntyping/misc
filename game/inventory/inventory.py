# Grid class
import grids
# Base item classes
import items

class InventoryError(Exception):
	"""An error in your Inventory! Oh noes!"""
	pass

class Inventory (grids.Grid):
	"""A grid that items can be placed into."""
	
	def findempty(self):
		"""Find an empty grid space"""
		for x,y in self:
			if not self(x,y):
				return x,y
		raise InventoryError("No empty grid spaces")

	# Giving items
	def give_item(self, item):
		"""Give an item to the inventory."""
		if issubclass(item.__class__, items.StackableItem):
			for x,y in self:
				if self(x,y).__class__ == item.__class__:
					item = self(x,y).merge(item)
					if item == None:
						return
		x,y = self.findempty()
		self(x,y,item)

	def place_item(self, item, coord = None):
		"""Place an item into the inventory."""
		x,y = coord
		if self(x,y):
			if self(x,y).__class__ == item.__class__:
				item = self(x,y).merge(item)
			else:
				original = self(x,y)
				self(x,y,item)
				return original
		else:
			self(x,y,item)
		return None

	# Taking items
	def take_item(self, coord):
		"""Take a single item from an inventory slot."""
		x,y = coord
		if issubclass(self(x,y).__class__, items.StackableItem):
			# If the item is a stack, take a single item from the top
			item = self(x,y).take_item()
			if self(x,y).stack <= 0:
				del self[(x,y)]
			return item
		else:
			return self.pickup_item(coord)
		
	def pickup_item(self, coord):
		"""Grab the entire contents of an inventory slot."""
		x,y = coord
		item = self(x,y)
		del self[(x,y)]
		return item

	# Moving items
	def move_item(self, coord1, coord2):
		return self.place_item( self.pickup_item(coord1), coord2 )
