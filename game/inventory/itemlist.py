import items

# Defined items

class Tool (items.Item):
	pass

class Axe (Tool):
	def activate(self):
		print "I hit it with my axe."

# Blocks

class Block (items.StackableItem):
	def activate(self):
		self.stack -= 1
		print "Block [%s] placed." % self.__class__.__name__

class Wood (Block):
	maxstack = 4

class Dirt (Block):
	maxstack = 5

class Stone (Block):
	maxstack = 3
