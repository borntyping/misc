class ItemError (Exception):
	pass

class Item:
	"""Base class for objects that can be placed into an inventory"""
	def __str__(self):
		return "%s" % self.__class__.__name__

class MenuItem:
	"""Provides menu features for an item."""
	menu = dict()

	def menu(self):
		

class StackableItem (Item):
	"""Basic stackable item class. Should not store unique data."""
	stack = 1
	maxstack = 10

	def __init__(self, stack = 1):
		self.stack = stack

	def isempty(self):
		if self.stack < 1:
			return True
			
	def add_item(self):
		"""Increase the stack by one."""
		if not self.maxstack > self.stack:
			raise ItemError("Stack full!")
		self.stack += 1

	def merge(self, item):
		"""Merges another item stack into itself."""
		if not self.__class__ == item.__class__:
			raise ItemError("Wrong item class, cannot merge.")

		emptyspace = (self.maxstack - self.stack)
		if emptyspace >= item.stack:
			self.stack += item.stack
			return None
		elif emptyspace < item.stack:
			self.stack = self.maxstack
			item.stack = (self.stack + item.stack) - self.maxstack
			return item

	def take_item(self):
		if not self.stack > 0:
			raise ItemError("Trying to take from empty stack.")
		self.stack -= 1
		return self.__class__(1)

	def __str__(self):
		return "%s [%s]" % (self.__class__.__name__, self.stack)
