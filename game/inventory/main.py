from inventory import *
import itemlist

"""Documentation!"""

def inv_test():
	inv = Inventory(3,3)

	# Put items into inventory
	inv.give_item(itemlist.Dirt(4))
	inv.give_item(itemlist.Stone(2))
	inv.give_item(itemlist.Dirt(4))
	inv.place_item(itemlist.Axe(),(1,1))

	print "# Inventory\n", inv

	a = inv.take_item((2,0))
	inv.place_item(a,(2,2))
	inv.move_item((1,1),(1,2))

	print "# Inventory\n", inv

def merge_test():
	a = itemlist.Dirt(2)
	b = itemlist.Dirt(1)
	b.merge(a)

	print a, b

def crafting_test():
	class Crafting:
		patterns = [ ((2,2),[[itemlist.Wood,itemlist.Wood],[itemlist.Wood,itemlist.Wood]],itemlist.Axe) ]
			
		def __init__(self, input, output):
			self.input = input
			self.output = output

		def match_patterns(self):
			for size, pattern, crafted_item in self.patterns:
				print size, self.input.size
				print "%s > %s" % (pattern, crafted_item)

	# Create an Inventory
	table = Inventory(2,2)

	# Create the crafting handler
	crafting = Crafting(table, Inventory(1,1))

	# Fill the table with wood
	for i,j in table:
		table.place_item(itemlist.Wood(),(i,j))

	crafting.match_patterns()


if __name__ == "__main__":
	#inv_test()
	crafting_test()
