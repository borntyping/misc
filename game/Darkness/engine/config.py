from pygame import *

class keys:
	KeyDown = KEYDOWN
	KeyUp = KEYUP

	Quit = K_ESCAPE
	Screenshot = K_F12
	NextWindow = K_TAB

class mods:
	LeftShift = KMOD_LSHIFT
	RightShift = KMOD_RSHIFT

class events:
	Quit = QUIT