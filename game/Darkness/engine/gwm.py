from lib.pygcurse import PygcurseSurface, PygcurseWindow
from config import keys, mods, events

class Window(PygcurseSurface):
	"""Base window class for windows use in the gwm."""
	def __init__ (self, width, height):
		PygcurseSurface.__init__(self, width, height)
		self.setscreencolors('white', 'black', clear=True)
	
	def input(self, event, modkeys = 0):
		self._properties.clean = False
		
	def draw(self):
		self.fill(' ')
		return self
	
	def message(self, message):
		self.putchars( message, x=int((self.width+len(message))/2), y=int(self.height/2) )
	
class WindowProperties():
	"""Holds the window data."""
	__slots__ = 'name', 'position', 'active', 'clean'
	def __init__(self, name, position, active = False, clean = False):
		self.name, self.position, self.active, self.clean = name, position, active, clean

class GameWindowManager():
	"""Manages the windows inside the main window."""
	
	def __init__ (self, game, size, font):
		width, height = size
		self.game = game
		self.mainwindow = PygcurseWindow(width, height, font = font)
		self.mainwindow.autoupdate = False
		self.aw = 0 # Active window index
		self.windows = list()
		self.debug = True
		self.debug_message = {}
		self.mainwindow.setscreencolors('white', 'blue', clear=True)
	
	def input(self, event, modkeys = 0):
		if event.type == keys.KeyDown:
			if event.key == keys.NextWindow:
				if modkeys & mods.LeftShift:
					self.next_window(-1)
				else:
					self.next_window()
			else:
				self.active_window.input(event, modkeys)
	
	def register_window(self, name, window, position, draw = True):
		"""Register a window with the window manager. Assigns a properties attribute to the window."""
		window._properties = WindowProperties(name, position, draw)
		self.windows.append(window)
	
	def get_window(self, name):
		for window in self.windows:
			if window._properties.name == name:
				return window
		return False
	
	def next_window(self, move = 1):
		self.active_window._properties.active, self.active_window._properties.clean = False, False
		self.aw += move
		if self.aw >= len(self.windows):
			self.aw = 0
		self.active_window._properties.active, self.active_window._properties.clean = True, False
		return self.aw
		
	def print_debug_info(self):
		self.debug_message['awn'] = self.active_window_name
		self.mainwindow.putchars(str(self.debug_message), x=3, y= self.mainwindow.height-3, fgcolor = 'green')
	
	def update(self):
		"""Update the window."""
		for window in self.windows:
			if not window._properties.clean:
				window.draw()
				self.draw_window( window )		
		if self.debug:
			self.print_debug_info()
		self.mainwindow.update()
	
	def draw_window(self, window):
		"""Draw one of the child windows."""
		x,y = window._properties.position
		window.paste((0, 0, None, None), self.mainwindow, (x, y, None, None))
		window._properties.clean = True
	
	"""Active window properties."""
	@property
	def active_window(self):
		return self.windows[self.aw]
	
	@property
	def active_window_name(self):
		try:
			return self.windows[self.aw]._properties.name
		except IndexError:
			return str(None)
	
	"""Returns size of availible drawing space."""
	@property
	def size(self): return self.width, self.height
	@property
	def width(self): return self.mainwindow.size[0]
	@property
	def height(self): return self.mainwindow.size[1]