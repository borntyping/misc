from gwm import Window

class World:
	def __init__(self, z, x, y):
		self.z, self.x, self.y = z, x, y
		
		grid = []
		for Z in range(z):
			z_slice = []
			for X in range(x):
				x_slice = []
				for Y in range(y):
					x_slice.append('.')
				z_slice.append(x_slice)
			grid.append(tuple(z_slice))
		self.grid = tuple(grid)
	
	@property
	def size(self): return self.x, self.y, self.z

class window(Window):
	def __init__ (self, width, height):
		Window.__init__(self, width, height)
		self.setscreencolors('green', 'black', clear=True)
		
		self.layer = 0
		self.world = False
	
	def changelayer(self, change):
		z = self.layer
		if change in ['up']:
			z = z-1
		elif change in ['down']:
			z = z-1
		elif isinstance(change, int):
			z = change
		if z < 0 or z >= len(self.world.grid):
			return False
		self.layer = z
	
	def load_world(self, world):
		print "Loading world."
		self.world = world
		self._properties.clean = False
	
	def draw(self):
		self.fill(' ')
		if self.world:
			z = self.layer
			for x in xrange( min(self.world.x, self.width) ):
				for y in xrange( min(self.world.y, self.height) ):
					self.putchars(str(self.world.grid[z][x][y]), x=x, y=y)
			return self
		else:
			self.message("No world loaded")