# Libraries
import pygame

# Modules
import config, gwm, menu, world

class Engine():
	def __init__(self, size):
		self.running = True
		self.gwm = gwm.GameWindowManager(self, size, "./fonts/monofur.ttf")
		
		menuwidth = 20
		self.gwm.register_window('menu', menu.window(menuwidth, self.gwm.height - 2, menu.DarknessMenu), (self.gwm.width - menuwidth - 1, 1))
		self.gwm.register_window('world', world.window(self.gwm.width - menuwidth - 3, self.gwm.height - 2), (1,1))
		
		self.gwm.update()
		self.gwm.get_window('world').load_world(self.load_world())

		self.mainloop()
		self.exit()
		
	def load_world(self):
		self.world = world.World(2,10,10)
	
	def exit(self):
		pygame.quit()
		exit()
	
	def mainloop(self):
		fps, time = 0, 0
		while self.running == True:
			start = pygame.time.get_ticks()
			self.main()
			self.gwm.debug_message['frame'] = pygame.time.get_ticks() - start
			fps += 1
			time += self.gwm.debug_message['frame']
			if time > 1000: # Count 1 second
				self.gwm.debug_message['fps'] = fps
				time, fps = time - 1000, 0
		self.exit()
	
	def main(self):
		for event in pygame.event.get():
			if event.type == config.events.Quit:
				self.running = False
			elif event.type == config.keys.KeyDown:
				# Keyboard input
				modkeys = pygame.key.get_mods()
				if event.key == config.keys.Quit:
					self.running = False
				elif event.key == config.keys.Screenshot:
					pygame.image.save(self.gwm.mainwindow._surfaceobj, "./data/screen.png")
				else:
					self.gwm.input(event, modkeys)
		self.gwm.update()