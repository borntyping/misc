from gwm import Window

DarknessMenu = {
	'Build': ('B', 'Place buildings', {
		'Flag': ('F', 'Place buildings', None),
		'Landing platform': ('L', 'Plaform for spaceships to land on', None)
	})
}

class window(Window):
	def __init__ (self, width, height, menu):
		Window.__init__(self, width, height)
		self.setscreencolors('white', 'black', clear=True)
		
		self.index = 0
		self.menu = menu
	
	def input(self, event, modkeys = 0):
		self._properties.clean = False
		
	def draw(self):
		self.fill(' ')
		self.putchars('Active window', 0, 0)
		return self