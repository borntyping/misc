import math
import random

class Star():
	def __init__(self, sector, location):
		self.sector = sector
		self.location = location
	
	def __str__(self):
		return "<%s instance at (%s,%s)>" % (self.__class__.__name__, self.location[0], self.location[1])
	
class SolarSystem(Star):
	def __init__(self, sector, location):
		Star.__init__(self, sector, location)
		self.tags = []
	
	def create(self):
		self.nearby = self.sector.find_nearby(self.location, 50, self.__class__)
		if len(self.nearby) > 5:
			for l in self.nearby:
				if 'crossroads' in self.sector.locations[l].tags:
					return
			self.tags.append('crossroads')
	
	def __str__(self):
		return "<%s instance at (%s,%s) %s %s>" % (self.__class__.__name__, self.location[0], self.location[1], self.tags, self.nearby)
	
class Sector():	
	def __init__(self, random = random.Random(), stars = 10, size = 250):
		self.locations = dict()
		self.random = random
		self.size = size
		self.center = size/2, size/2
		
		# Place stars
		self.create_stars(50)
		self.create_solar_systems(50)
	
	def _random_location(self):
		return self.random.randint(-self.size + (self.size*0.1), self.size + 1 - (self.size*0.1))
	
	def random_location(self):
		"""Returns a random location coordinate."""
		return (self._random_location(), self._random_location())
	
	def create_stars(self, num):
		for i in xrange(num):
			location = self.random_location()
			self.locations[ location ] = Star(self, location)

	def create_solar_systems(self, num):
		for i in xrange(num):
			location = self.random_location()
			self.locations[ location ] = SolarSystem(self, location)
		for l in self.locations:
			if isinstance( self.locations[l], SolarSystem ):
				self.locations[l].create()
	
	def count(self, obj_class):
		"""Count the number of locations in the system that match the class"""
		count = 0
		for star in self.locations:
			if isinstance(self.locations[star], obj_class):
				count += 1
		return count
	
	def distance(self, a, b):
		return math.hypot(a[0] - b[0], a[1] - b[1])
	
	def find_nearby(self, point, distance, obj_class = None):
		"""Find nearby locations from a point, optionally of only a certain class."""
		count = []
		for l in self.locations:
			if self.distance(point, l) < distance:
				if obj_class == None or isinstance(self.locations[l], obj_class):
					count.append(l)
		return count
	
	def __str__(self):
		"""Print all the locations in this sector."""
		string = "Sector:"
		for l in self.locations:
			string += "\n\t%s" % self.locations[l]
		return string

if __name__ == '__main__':
	universe = Sector()
	print universe