import pygame
from galaxy import Sector

universe = Sector()

def s(coord, offset):
	return (coord[0]+offset[0],coord[1]+offset[1])
		
size = (universe.size,universe.size)
background = pygame.Surface(s(size,size))
background.fill((0,0,0))

for l in universe.locations:
	if universe.locations[l].__class__.__name__ == "SolarSystem":
		if 'crossroads' in universe.locations[l].tags:
			pygame.draw.circle(background, (0,255,255), s(l,size), 3, 1)
			pygame.draw.circle(background, (255,0,0), s(l,size), 50, 1)
		else:
			pygame.draw.circle(background, (0,255,255), s(l,size), 2, 0)
	else:
		pygame.draw.circle(background, (255,255,255), s(l,size), 2, 0)
		
pygame.image.save(background, 'data/sector.png')