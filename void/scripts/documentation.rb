#!/usr/bin/ruby1.8

# Uses watchr to recompile tex files with pdflatex when changes are made
# Should be run from the top of the repository

require 'rubygems'
require 'watchr'

def info ()
	system("echo && date")
end

def pdflatex (file, draftmode=false)
	draftmode = draftmode ? "-draftmode " : ""
	system("pdflatex -shell-escape -halt-on-error #{draftmode}'#{file}'")
end

def clean (file)
	Dir.glob("#{file}.{aux,ent,log,out,toc}").each do |f|
		system("rm", "#{f}")
	end
end

def process (directory, file)
	Dir.chdir(directory) do
		info()
		pdflatex(file, true) and pdflatex(file)
		clean(file)
	end
end

process("design", "Void.tex")

if (ARGV[0] == "--watch") then
	script = Watchr::Script.new
	script.watch("(design)/(.*)\.tex") {|m| process(m[1], m[2])}
	
	Watchr::Controller.new(script, Watchr.handler.new).run
end