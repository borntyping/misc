Void
====

A design for a multiplayer video-game, where players take the roles of crew on a spaceship and cooperatively control it.

Intrested?
----------

Contact me - `npc` in `#os` on `AFNet`, `sam@borntyping.co.uk`, `http://github.com/borntyping/void`.
