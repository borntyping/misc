\documentclass[a4paper]{article}
\usepackage[cm]{fullpage}
\usepackage[parfill]{parskip}

% Colors
\usepackage{color}
\usepackage[usenames,svgnames,table]{xcolor}

% Hyperref and pdf metadata
\usepackage[
	pdfauthor={Sam Clements},
	pdftitle={Void},
	colorlinks=true,
	urlcolor=RoyalBlue,
	linkcolor=Navy,
	linktoc=all
]{hyperref}

% Fonts
\usepackage{kpfonts}
\renewcommand*\familydefault{\sfdefault}
\usepackage{amsfonts}
\input Starburst.fd

% Notes
\usepackage{endnotes}
\usepackage{hyperendnotes}
\let\footnote\endnote

\usepackage[draft,layout={footnote}]{fixme}
\fxusetheme{color}
\fxusetargetlayout{plain}
\makeatletter
\renewcommand*{\FXLayoutMargin}[3]{\marginpar{\raggedright\footnotesize\color{fx#1} #2}}
\renewcommand*{\FXLayoutFootnote}[3]{\footnote{\textcolor{fx#1}{#2}}}
\renewcommand\listoffixmes{\section{Notes that need attention}\@starttoc{lox}}
\makeatother

% Useful commands
\newcommand{\void}{\textbf{\textsc{Void}}}
\newcommand{\repo}{\url{https://github.com/borntyping/void}}
\newcommand{\version}{\ifnum\pdfshellescape=1\texttt{\input{|"git describe"}}\fi} % Show version number when available

\begin{document}

\begin{titlepage}
	\null
	\vfill
	\begin{center}
		\fontsize{128pt}{0pt}
		\usefont{U}{Starburst}{xl}{n}
		\textcolor{MidnightBlue}{V\hspace{-12pt}oid}
	\end{center}
	\vfill
	\begin{center}
		A design for a game by \textbf{Sam Clements}
		
		\version
		
		\repo
	\end{center}
	\vfill
	\hypersetup{linkcolor=Black}
	\tableofcontents
	\vfill
\end{titlepage}
\newpage

% --------------------------------------------------
\newpage\part{Introduction}
% --------------------------------------------------

\section{Introduction}

	\void{} is a space exploration and combat game, where multiple players take the roles of crew members on a single ship - pilots, engineers, officers and captains - and all contribute to the same goals. They travel through space, in an open, procedurally generated world.
	
	The inspiration for this game pulls from media such as {\em Star Trek}, {\em Firefly} and {\em Artemis}. It's intended to create a game that both has detailed gameplay, and allows and encourages emergent storytelling as a result of the players actions.
	
	\subsection{Gameplay}

		Each player uses one of the ship's available ``Control Stations``, which each make a set of the ships controls available to the player. With players each controlling different parts of the ship, the crew must cooperate to control the ship effectively, both while travelling and during combat.
		
		In the early versions of the game, the gameplay areas will be limited to small, fixed sized areas such as solar systems or other areas that are (relatively) densely populated. Later on in the games development, this could be expanded to 
		larger persistent universes, encouraging longer term games -- and stories.
	
	\subsection{End Goals}
	
	The project has several major end goals - though this does not mean that development of the game may go further once they are completed.

	\begin{itemize}
		\item Detailed gameplay for multiple player roles, making experienced players a valuable addition to a crew.
		\item Open, persistent worlds, with the majority of the universe procedurally generated.
		\item Multiple NPC factions, providing opportunities for missions, trading and combat.
	\end{itemize}

\section{Technical Details}

	The game will be written in \href{http://www.python.org/}{Python 2.7}, with the possibility of extracting some of the code into C/C++.
	\href{http://www.gevent.org/}{gevent} is the current candidate for handling networking, and \href{http://www.panda3d.org/}{Panda3D} is the current candidate for the client side rendering and physics.\fxerror{Are there any better graphics, networking and physics engines that could be used?}
	
	\begin{quote}
		\textbf{Why Python?}
		Because it's easy for developers to learn and express themselves in.
		Python 2.7 will be used instead of the more recent Python 3.X, due to a wide range of libraries, and support for the major candidate tools (gevent and Panda3D).
	\end{quote}
	
	Development will be managed using the \href{http://git-scm.com/}{git} version control software, the \href{https://github.com/}{github} service and the \href{http://nvie.com/posts/a-successful-git-branching-model/}{git-flow} development model.
	All of these tools work on both Linux and Windows, and so the game should be (hopefully) platform independent.
	
	\begin{quote}
		\textbf{Why Git?}
		It's fast, decentralised, has the github service and is generally regarded as the best version control out there, among a whole host of other reasons.
		Take a look at \url{http://git-scm.com/about} if you still need convincing.
	\end{quote}
		
\section{Contributing}

		The project isn't only going to need programmers - as development progresses, artists, designers, documenters and finally, testers, will be useful.
		If there are enough people interested in keeping an eye on the project, and finding out how they could contribute, a mailing list may be set up.
	
% --------------------------------------------------
\newpage\part{Game mechanics}
% --------------------------------------------------

\section{Factions, Ships and Crews}

	Ships -- and other controlled entities, such as space stations -- will each belong to a {\bf faction}.
	Ships in the same faction will have advantages when interacting with each other, such as shared communications frequencies and compatible hardware.
	
	Entities such as abandoned ships and stations will use a `default faction'\fxnote{Is there a better name for the default faction?}, and will not receive any of the advantages a faction would normally grant.

\section{Control Stations}

	Ships are divided into {\bf sections}, containing {\bf control stations}. 
	Each control station can be accessed by a player, allowing them to use the set of {\bf control panels} the station provides.\fxwarning{What can be done for groups of panels which could easily be represented by a single panel?}
	Players can move to other stations, but this takes time, and moving to a station in another section of the ship takes even longer.
	
	All ships will have a different configuration of sections and stations. Small ships may be able to be controlled by a small number of players, or even an individual, but larger ships will require larger crews before they can be controlled easily.

	\begin{quote}\color[gray]{0.3}
		For example, the ship {\em Serenity} from {\em Firefly} has three major stations, across several sections: the two pilot seats -- in the bridge section -- where the controls would include motion and communications and the engine room -- a section of it's own -- which would have controls for power management.
		Other stations would include the Medical and Cargo bays, which would only be used when needed.
		With only a few stations, the ship would only be suitable for a small crew, though this may be an advantage in some situations.
	\end{quote}
	
	Currently, the various panels and game mechanics are grouped into four sections: \hyperref[motion]{Motion}, \hyperref[engineering]{Engineering}, \hyperref[weaponry]{Weaponry} and \hyperref[sensors]{Sensors}.

	\subsection{Motion: Warp, Manoeuvre and Impulse}
	\label{motion}

	\subsection{Engineering: Fuel, Power and CPU}
	\label{engineering}

	\subsection{Weaponry: Launchers and Turrets}
	\label{weaponry}

	\subsection{Sensors: Scanning, Electronic Warfare and Communications}
	\label{sensors}

% --------------------------------------------------
\newpage\part{Appendixes}
% --------------------------------------------------

\appendix

\def\enoteheading{\section{\notesname}}
\begingroup
	\parindent 0pt
	\parskip 0ex
	\def\enotesize{\normalsize}
	\theendnotes
\endgroup

\end{document}
