<?php

switch ($command) :
	case 'info' :
		isadmin($nick,1,$message);
		$say = "";
		$say .= "Botnick: '$botnick'; ";
		$say .= "Sendto: '$sendto'; ";
		$say .= "Message: '$message'; ";
		send("PRIVMSG {$sendto} :{$say}");
		break;
	case 'roll' :
		// Format as AdB+C
		$rolls = explode("roll ",$message,2);
		$rolls = explode(' ',$rolls[1]);
		foreach ($rolls as $dice) :
			$i = 0;
			$roll['dice']		= 1;
			$roll['sides']	= 6;
			$roll['mod']		= 0;
			$dice = str_replace("d+","d".$roll['sides']."+",$dice);
			$tok = strtok($dice, "d");
			while ($tok !== false) :
				switch ($i) :
					case 0 : $roll['dice']  = $tok; break;
					case 1 : $roll['sides'] = $tok; break;
					case 2 : $roll['mod']   = $tok; break;
				endswitch;
				$i++;
				$tok = strtok("+");
			endwhile;
			if ($roll['mod'] == 0) :
				$dieroll = $roll['dice']."d".$roll['sides'];
			else :
				$dieroll = $roll['dice']."d".$roll['sides']."+".$roll['mod'];
			endif;
			$i = 0;
			$resultrolls = array();
			while ($i != $roll['dice']) :
				$resultrolls[$i] = mt_rand(1,$roll['sides']);
				$i++;
			endwhile;
			$total = array_sum($resultrolls) + $roll['mod'];
			$resultrolls = implode($resultrolls,' ');
			if ($commandtype == 'private'){
				send("PRIVMSG {$sendto} :You roll {$dieroll} and get {$total} [{$resultrolls}]");
				send("PRIVMSG ".CHANS." :{$nick} rolled some dice.");
			} else {
				send("PRIVMSG {$sendto} :{$nick} rolls {$dieroll} and gets {$total} [{$resultrolls}]");
			}
		endforeach;
		break;
	case 'say' :
		isadmin($nick,1,$message);
		$say = explode("say ",$message,2);
		if(!isset($say[1]))
			send("PRIVMSG {$nick} :Try giving me something to say.");
		if ($commandtype == 'private') :
			$say = explode(" to ",$say[1],2);
			if(!isset($say[1]))
				$say[1] = CHANS;
			send("PRIVMSG {$say[1]} :{$say[0]}");
		else :
			send("PRIVMSG {$sendto} :{$say[1]}");
		endif;		
		break;
	case 'die' :
		isadmin($nick,1,$message);
		send("PRIVMSG {$sendto} :Fine. I'll go die.");
		send("QUIT :");
		die();
		break;
	default:
		$message = $shortmessage; // Removes bot nickname from start
		include("respond.php");
		break;
endswitch;

?>
