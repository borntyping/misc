#!/usr/bin/php

<?php

set_time_limit(0);


if(isset($argv[1])) {
	define('NICK', $argv[1]);
} else {
	define('NICK', 'Aleph');
}

// Config
define('SERVER',		'irc.sorcery.net');
// define('SERVER',		'borntyping.local');
define('PORT',			'6667');
define('IDENT', 		'Aleph');
define('GECOS',			'Ziaix\'s pet bot'); // Real name
define('NSPASS',		'');
define('CHANS',			'#testing');
define('ADMIN',			'Ziaix');
define('ADMINCODE',	'nevergonnagiveyouup');

// Formatting - Change these and they'll break
define('C_BOLD',			chr(2));
define('C_COLOR',			chr(3));
define('C_ITALIC',		chr(29));
define('C_REVERSE',		chr(22));
define('C_UNDERLINE',	chr(31));
define('B', "\n");

// Variable setting
$read = null; // For reading in data
$sock = null; // Socket
$timers = array();
$timers['ping'] = 0;

$botnick = NICK;
echo ">>> I am ".$botnick."\n";

// Send Function
function send($message) {
	global $sock;
	fputs($sock, "{$message}\n");
	echo "<- :self: {$message}".B;
}

function isadmin($nick,$security,$tell) {
	if ($nick != ADMIN) {
		send("PRIVMSG {$nick} :Leave me alone!");
		if ($security == 1) {
			send("PRIVMSG {$nick} :I'm telling daddy!");
			send("PRIVMSG ".ADMIN." :WARNING: {$nick} sent \"{$message}\" to {$botnick}");
		}
		continue;
	}
}

// Loop forever
while (1)
{
	$sock = fsockopen(SERVER, PORT);
	if (!$sock)
	{
		echo "\n".'Error connecting to '.SERVER.':'.PORT.'! Quiting now.'."\n"; 
		die();
	}
	else
	{

		// Stop the stream blocking [TAG:?]
		stream_set_timeout($sock, 0, 1);
		stream_set_blocking($sock, 0);

		// Login
		// send('NS IDENTIFY ' . NSPASS);
		send('USER '.IDENT . ' borntyping.co.uk * :' . GECOS);
		send('NICK '.NICK);
		send('JOIN '.CHANS);

		// Loop until socket dies
		while (!feof($sock))
		{
			echo "Logged on!\n";
	
			// Read delay of 0.05 second
			// Reduce this to increase the responsiveness of the bot in busy channels
			// Be aware that it'll raise CPU load
			usleep(100000);

			// Increment all timers
			foreach ($timers as $key => $timer)
			{
				$timers[$key]++;
			}

			$read .= fgets($sock, 512);
			while (substr_count($read, "\n") != 0)
			{
				$offset = strpos($read, "\n");
				$data = substr($read, 0, $offset);
				$read = substr($read, $offset + 1);
				$dataSplit = explode(' ', $data);
				if ($dataSplit[0] != 'PING')
				{
					echo "-> {$data}".B;
					flush();
				}
				
				if ($dataSplit[0] == 'PING')
				{
					// PINGs
					fputs($sock, 'PONG '.substr($data, 5).'\n');
				}
				elseif ($dataSplit[1] == 'PRIVMSG')
				{
					// Regular message handler
					// Get the required info from the message
					// Such as
					// :Ziaix!Ziaix@287c9d63.2835306f.com.hmsk PRIVMSG #test :Aleph: hello
						
					// Everything after the #room name
					// Aleph: say hi
					$message = trim(ltrim(implode(' ', array_slice($dataSplit, 3)), ':'));
					$shortmessage = trim(ltrim(implode(' ', array_slice($dataSplit, 4)), ':'));
			
					// Puts message into an array
					// 0 => 'Aleph:', 1 => 'hello'
					$words = explode(' ', trim($message));
					$wordcount = count($words);
					$lastword = $words[$wordcount-1];
			
					// Who the message is to
					// #room or user
					$to = $dataSplit[2];
			
					// Senders Nick
					$nick = explode('!', $dataSplit[0]); // ":This![...]"
					$nick = ltrim($nick[0], ':');

					// Private messages
					if ($words[0] == "die#".ADMINCODE)
					{
						isadmin($nick,1,$message);
						send("QUIT");
						die();
					}
					elseif (strtolower($to) == strtolower($botnick))
					{
						$commandtype = 'private';
						$command = $words[0];
						$sendto = $nick;
						include("sharedcommands.php");
						include("privatecommands.php");
					}
					elseif ($words[0] == $botnick.":")
					{
						$commandtype = 'room';
						$command = $words[1];
						$sendto = $to;
						include("sharedcommands.php");
						include("commands.php");
					}
					elseif ($lastword = $botnick)
					{
						$message = str_replace(" $botnick","",$message);
						include("respond.php");
					}
			
				}
				elseif ($dataSplit[1] == 'NOTICE')
				{
					// Get the required info from the message
					$message = trim(ltrim(implode(' ', array_slice($dataSplit, 3)), ':'));
					$words = explode(' ', trim($message));
					$to = $dataSplit[2];
					$nick = explode('!', $dataSplit[0]);
					$nick = ltrim($nick[0], ':');
		/*
					if (strtolower($to) == strtolower($botnick)) :
					// Private notices
						// Your code here
					else :
					// Channel notices
						// Your code here
					endif;
		*/
				}
			}
		}
	}
}
?>
