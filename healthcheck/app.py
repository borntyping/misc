from flask import Flask, render_template, request
import requests

app = Flask(__name__)
app.config['GRAPHITE'] = 'http://ec2-46-137-152-79.eu-west-1.compute.amazonaws.com'

@app.route("/")
def select():
    response = requests.get(app.config['GRAPHITE'] + '/render?target=http_tester.*.success&from=-6hours&format=json')
    targets = [series['target'].split('.')[1] for series in response.json()]
    return render_template("select.html", targets=targets, graphite=app.config['GRAPHITE'])
    
@app.route("/target/<string:target>")
def show(target):
    status=dict()
    url = '{base}/render?target=http_tester.{target}.*&from=-1minute&format=json'.format(
        base=app.config['GRAPHITE'], **locals())
    response = requests.get(url)
    
    status = dict()
    for d in response.json():
        section = d['target'].split('.', 2)[-1]
        section_status = d['datapoints'][-1][0] == 1
        status[section] = section_status

    return render_template("show.html", target=target, graphite=app.config['GRAPHITE'], **status)
    
@app.route("/new", methods=['GET', 'POST'])
def new():
    if request.method == 'POST':
        id, url, text = request.form['id'], request.form['url'], request.form['text']
        with open("input.txt", 'a') as urlfile:
            urlfile.write("{id},{url},{text}\n".format(**locals()))
        return render_template("new_complete.html", **locals())
    return render_template("new.html")

if __name__ == "__main__":
    app.run(host='ec2-46-137-152-79.eu-west-1.compute.amazonaws.com', port=5000, debug=True)
