#!/usr/bin/python
# 0-59 * * * * python $HOME/healthcheck/main.py $HOME/healthcheck/input.txt localhost 2003

import sys
import socket
from urllib2 import urlopen, URLError
from argparse import ArgumentParser
from time import time

class TestUrl(object):
    def __init__(self, line):
	    self.id, self.url, self.text = line.strip().split(',')

    def __repr__(self):
	    return self.id

def check(testurl):
    """Fetch a test url, and return data to be sent to graphite"""
    results = dict()
    template  = "http_tester.{id}.page_fetched {page_fetched} {time}\n"
    template += "http_tester.{id}.success {success} {time}\n"
    template += "http_tester.{id}.time {request_time} {time}\n"
    
    try:
        start = time()    
        r = urlopen(testurl.url)
        text = r.read()
    except URLError as e:
        # Failed to fetch url
        results['page_fetched'] = int(False)
    else:
        # Fetched url
        results['page_fetched'] = int(True)
        
        results['string_matched'] = int(text.find(testurl.text) != -1)
        results['age'] = r.info().get('Age', None)
        template += "http_tester.{id}.string_matched {string_matched} {time}\n"
        template += "http_tester.{id}.http_header_age {age} {time}\n"
    finally:
        results['request_time'] = time() - start
        results['success'] = int(results.get('page_fetched', False) and results.get('string_matched', False))
        
    return template.format(id=testurl.id, time=time(), **results)

parser = ArgumentParser()
parser.add_argument("urlfile")
parser.add_argument("host")
parser.add_argument("port", type=int, nargs='?', default=2003)
parser.add_argument("--timeout", type=int, default=10)
parser.add_argument("--show", action='store_true')

def main():
    args = parser.parse_args()
    
    socket.setdefaulttimeout(args.timeout)

    try:
        with open(args.urlfile, 'r') as urlfile:
            testurls = [TestUrl(line) for line in urlfile]
    except:
        print >> sys.stderr, "Could not read urlfile"
        exit(2)
        
    data = ""
    for testurl in testurls:
        print "Fetching {id} ({url})...".format(id=testurl.id, url=testurl.url)
        data += check(testurl)
        
    if args.show:
        print data
    else:
        try:
            print "Sending results to graphite..."
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((args.host, args.port))
            s.sendall(data)
            s.close()
        except socket.timeout:
            print >> sys.stderr, "Could not connect to graphite"
            exit(1)

if __name__ == "__main__":
	main()
