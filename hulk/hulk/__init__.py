"""	Hulk: generate static HTML sites from templates and pages in multiple markup langauges """

from __future__ import absolute_import

__version__ = '0.1.0'

import os, shutil
import logging
import colorlog
from jinja2 import Environment, FileSystemLoader
from argparse import ArgumentParser

def configure ():
	parser = ArgumentParser(prog='hulk')
	parser.add_argument('-v', '--version', action='version', version=__version__)
	
	parser.add_argument('--log', type=str, metavar='LOG', default='INFO',
		help="logging level, defaults to INFO")
	
	args = parser.parse_args()
	
	return args
	
class FileError (Exception):
	pass
	
class File (object):
	def __init__ (self, source_dir, output_dir, relpath):
		"""
		Sets the files basic attributes
		
		`static`, `header` and `content` are only set after `parse()` is called.
		"""
		self.relpath = relpath
		self.source_path = os.path.join(source_dir, relpath)
		self.output_path = os.path.join(output_dir, relpath)
		
		self.extension = os.path.splitext(relpath)[1][1:]
	
	def render (self):
		pass
	
	def __str__ (self):
		return "{}({}, {})".format(self.__class__.__name__, self.name, self.extension)

class Page (object):
	def render (self):
		pass

class Hulk (object):
	def __init__ (self):
		self.config = configure()
		
		self.setup_logging(level=self.config.log.upper())
		self.setup_directories()
		self.setup_jinja2()
		
	def setup_logging (self, level):
		"""	Create a logger """
		log_format = "%(log_color)s%(levelname)8s%(reset)s %(message)s"
		
		handler = logging.StreamHandler()
		handler.setFormatter(colorlog.ColoredFormatter(log_format, {
			'DEBUG':    'white',
			'INFO':     'blue',
			'WARNING':  'yellow',
			'ERROR':    'bold_red',
			'CRITICAL': 'bg_bold_red',
		}))
		
		self.log = logging.getLogger('hulk')
		self.log.setLevel(getattr(logging, level))
		self.log.addHandler(handler)
	
	def setup_directories (self):
		self.source_dir = os.getcwd()
		self.log.info("Input directory:  %s" % self.source_dir)
		
		self.output_dir = os.path.join(self.source_dir, '_site')
		self.log.info("Output directory: %s" % self.output_dir)
		
		# Remove the output directory if it already exists
		if os.path.exists(self.output_dir):
			if os.path.isdir(self.output_dir):
				self.log.warning("Removing old output directory")
				shutil.rmtree(self.output_dir)
			else:
				raise FileError("There is a file in the output location.")
		
		# Create the output directory
		os.mkdir(self.output_dir)
	
	def setup_jinja2 (self):
		self.env = Environment(loader=FileSystemLoader('_templates'))
	
	def process (self, file):
		"""
		Work out if a file has a header section or not,
		then process it appropriately.
		"""
		with open(file.original_path, 'r') as f:
			static = not (f.readline().strip() == File.DELIMITER)
		
		if static:
			self.process_file(file)
		else:
			self.process_page(file)
	
	def get_output_path (self, relative_path):
		"""	Get the output path for a file or directory's relative path """
		return os.path.join(self.output, relative_path)
	
	
	DELIMITER = '---'
	
	def process_page (self, file):
		"""	Process a file with a header section """
		self.log.info("Parsing " + file.relative_path)
		
		# Seperate the header and contents
		with open(file.original_path, 'r') as f:
			headers = []
			delimeters = 0
			while delimeters < 2:
				line = f.readline().strip()
				if line == '':
					raise FileError("Encountered end of file before the header was closed.")
				elif line == self.DELIMITER:
					delimeters += 1
				else:
					headers.append(line)
			content = f.read()
		
		with open(self.get_output_path(file.relative_path), 'w') as f:
			f.write(content)
	
	def process_file (self, file):
		"""	Copy a static file to the output directory """
		self.log.info("Copying " + file.relative_path)
		shutil.copyfile(file.original_path, self.get_output_path(file.relative_path))
	
	def main (self):
		self.files = list()
		
		for (directory, subdirs, files) in os.walk(self.source_dir, followlinks=True):
			# The path for the current directory
			directory_path = os.path.join(self.source_dir, directory)
			directory_relative_path = os.path.relpath(directory_path, self.source_dir)
			
			self.log.debug("Directory: " + directory_relative_path)
			
			# Skip the root directory...
			if not directory == self.source_dir:
				# Create each of the subdirectories in the output directory
				os.mkdir(self.get_output_path(directory_relative_path))
			
			# Skip all directories where the name starts with an underscore
			for d in subdirs:
				if os.path.basename(d)[0] == '_':
					subdirs.remove(d)

			for filename in files:
				original_path = os.path.join(directory, filename)
				relative_path = os.path.relpath(original_path, self.source_dir)
				
				with open(original_path, 'r') as f:
					is_page = (f.readline().strip() == self.DELIMITER)
				
				self.files.append((Page if is_page else File)(self.source_dir, self.output_dir, relative_path))
			
if __name__ == "__main__":
	Hulk().main()