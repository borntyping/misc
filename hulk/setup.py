#!/usr/bin/env python

from distutils.core import setup
from hulk import __version__

setup(
    name             = 'hulk',
    version          = __version__,
    
    description      = 'Static site generator, inspired by Jekyll and Hyde.',
    
    author           = 'Sam Clements',
    author_email     = 'sam@borntyping.co.uk',
    url              = 'https://github.com/borntyping/hulk',
    
    packages         = ['hulk'],
    scripts          = ['scripts/hulk'],
    requires         = ['colorlog (>=1.0)', 'Jinja2 (>=2.6)'],
    
    classifiers      = [
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 3',
        'Topic :: Internet :: WWW/HTTP'
        'Topic :: Terminals',
        'Topic :: Text Processing',
        'Topic :: Text Processing :: Markup',
		'Topic :: Text Processing :: Markup :: HTML',
    ],
)
