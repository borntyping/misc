class strings:
	pass

strings.system = "{self.name} ~ costs {self.cost}K"
strings.armour = strings.system + " ~ Provides {self.dr} dDR"

class system:
	"""A single spacecraft system"""
	def __init__(self,name,cost):
		self.name = name
		self.cost = cost
		
	def __repr__(self):
		return strings.system.format(self=self)

class armour(system):
	"""An armour system"""
	def __init__(self,name,cost,dr):
		self.name = name
		self.cost = cost
		self.dr   = dr
	
	def __repr__(self):
		return strings.armour.format(self=self)
