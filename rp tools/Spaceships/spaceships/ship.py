class ship:
	"""Ship with front, central and rear hulls."""
	
	def __init__(self,size):
		self.f = self.front   = hull('Front')
		self.c = self.central = hull('Central')
		self.r = self.rear    = hull('Rear')
		self.features = list()
		self.size = size
	
	def __str__(self):
		description = "Ship with front, central and rear hulls."
		for hull in self:
			description += '\n\n' + str(hull)
		return description
	
	def __iter__(self):
		self.index = 0
		return self

	def next(self):
		self.index = self.index + 1
		if self.index == 4:
			raise StopIteration
		if self.index == 1: return self.front
		if self.index == 2: return self.central
		if self.index == 3: return self.rear	

	def getarmour(self):
		total = 0
		for hull in self:
			total += hull.getarmour()
		return total
