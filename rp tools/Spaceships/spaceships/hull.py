#from systems import system

class hull:
	"""A hull section containing systems."""
	
	def __init__(self,name):
		self.name = name
		self.systems = list()
	
	def __repr__(self):
		description = self.name + ' hull'
		for system in self.systems:
			description += '\n-- ' + str(system)
		return description
		
	def __iter__(self):
		return self.systems
		
	def add(self, systemobject):
		self.systems.append(systemobject)
	
	def getarmour(self):
		total = 0
		for system in self.systems:
			if str(system.__class__) == 'spaceships.systems.armour':
				total += system.dr
		return total
