from django.conf.urls.defaults import *

character_urlpatterns = patterns('virtualcharactercreator.views.character',
	(r'^$', 'index'),
	(r'^(?P<char_id>\d+)/$', 'detail'),
	(r'^(?P<char_id>\d+)/edit/$', 'edit'),
)

urlpatterns = patterns('',
	(r'^$', 'index'),
	(r'^character/$', include(character_urlpatterns)),
)
