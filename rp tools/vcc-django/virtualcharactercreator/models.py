from django.db import models

class Character(models.Model):
	name = models.CharField("Characters name",max_length=200)
	owner = models.CharField("Players name",max_length=200)

	advantages = models.ManyToManyField("Advantage")

	def __unicode__(self):
		return "%s (%s)" % (self.name, self.owner)

class Advantage(models.Model):
	name = models.CharField("Advantage",max_length=200)
	base_cost = models.IntegerField("Cost per level")
	level_cost = models.IntegerField("Cost per level")

	#modifiers = models.ManyToManyField("Advantage")

	def __unicode__(self):
		return "%s (%s)" % (self.name, self.owner)
