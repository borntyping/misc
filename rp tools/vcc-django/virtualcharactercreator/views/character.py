from django.http import HttpResponse
#from django.shortcuts import render_to_response

from virtualcharactercreator.models import Character

def index(request):
	characters = Character.objects.all()
	output = ', '.join([c.name for c in characters])
	return HttpResponse(output)

def detail(request, char_id):
	return HttpResponse("Character %s" % char_id)

def edit(request, char_id):
	return HttpResponse("Editing character %s" % char_id)
