#!/usr/bin/python

import sys, pygame, subprocess
from grid import *

# Defaults
gridsize = (5,5)
spacing = 32
backgroundimg = 'map.png'

# Command line args
args = sys.argv[1:]
args.reverse()

while 1:
	try:
		arg = args.pop()
	except IndexError:
		break

	if arg == '--size':
		try:
			gridsize = int(args.pop()), int(args.pop())
		except IndexError:
			print '--size requires two arguments'
		except ValueError:
			print '--size requires two integers'
	elif arg == '--spacing':
		try:
			spacing = int(args.pop())
		except ValueError:
			print '--spacing requires an integer'
	elif arg == '--img':
		try:
			backgroundimg = arg
		except IndexError:
			print '--img requires a string'

# Startup!
pygame.init()
pygame.display.set_caption('SimpleMap')

grid = grid(spacing, gridsize)
size = width, height = grid.size
#size = width, height + 20 + grid.s
screen = pygame.display.set_mode(size)
print 'Size: ' + str(size)

background = pygame.Surface(screen.get_size())
background = background.convert()
background = pygame.image.load(backgroundimg)
#pygame.transform.crop(background,grid.size)
#background.fill(white)

# grid( spacing, width/hieght in tiles, origin point )
grid.draw(screen)
grid.draw_darkness(screen)
grid.draw_tokens(screen)

class opt: pass
opt.draw_darkness = True
opt.draw_tokens = True
opt.mode = False
opt.cuticons = False
opt.currenticon = '/usr/share/icons/oxygen/64x64/apps/akonadi.png'

def on(bool):
	if bool == True:
		return 'on'
	else:
		return 'off'

print "SimpleMap!"
print
print "'d' switches darkness, 'D' darkens all."
print "'x' switches token cutting."
print "'q' quits."
print "Spacebar switches beenween darkness and token modes."
print "Keys 1 and 2 activate tokens"

while 1:
	screen.blit(background, (0,0))
	screen.blit(grid.surface, (0,0))
	if not opt.draw_tokens == False:
		screen.blit(grid.tokens, (0,0))
	if not opt.draw_darkness == False:
		screen.blit(grid.darkness, (0,0))

	#for event in pygame.event.get():
	event = pygame.event.wait()
	if (event.type == pygame.QUIT) or ((event.type == pygame.KEYDOWN) and (event.key == pygame.K_q)):
		sys.exit()
	elif (event.type == pygame.KEYDOWN):
		mods = pygame.key.get_mods()
		if (event.key == pygame.K_1):
			opt.currenticon = '/usr/share/icons/oxygen/64x64/apps/akonadi.png'
		elif (event.key == pygame.K_2):
			opt.currenticon = '/usr/share/icons/oxygen/64x64/apps/ktorrent.png'
		elif (event.key == pygame.K_d) and (mods & pygame.KMOD_SHIFT):
			grid.darkness.fill(grey(50))
		elif (event.key == pygame.K_d):
			opt.draw_darkness = not opt.draw_darkness
			print 'Darkness is '+on(opt.draw_darkness)
		elif (event.key == pygame.K_x):
			opt.cuticons = not opt.cuticons
			print 'Cutting is '+on(opt.cuticons)
		elif (event.key == pygame.K_SPACE):
			opt.mode = not opt.mode
			print 'Mode is '+on(opt.mode)
		elif (event.key == pygame.K_s):
			pygame.image.save(screen, 'simplemap.png')
			p = subprocess.Popen('./imgur.py')
	elif (event.type == pygame.MOUSEBUTTONDOWN):
		if grid.rect.collidepoint(event.pos):
			for square in grid.squares:
				if square[0].collidepoint(event.pos):
					#print 'Square '+str(square[1])+' clicked'
					position = square[1][0] * grid.spacing, square[1][1] * grid.spacing
					x = square[1][0] * grid.spacing
					y = square[1][1] * grid.spacing
					if opt.mode == True:
						if square[1] not in grid.tokensquares:
							grid.tokensquares[square[1]] = pygame.Surface((grid.s,grid.s)), opt.currenticon
							grid.tokensquares[square[1]] = pygame.image.load(opt.currenticon), opt.currenticon
							grid.tokensquares[square[1]][0].set_colorkey(transparent)
							grid.tokens.blit( grid.tokensquares[square[1]][0], (x,y) )
						else:
							if opt.cuticons == True:
								opt.currenticon = grid.tokensquares[square[1]][1]
							del grid.tokensquares[square[1]]
							grid.tokens.fill(transparent, square[0])
					else:
						if square[1] not in grid.darksquares:
							grid.darksquares[square[1]] = pygame.Surface((grid.s,grid.s))
							grid.darksquares[square[1]].fill(grey(75))
							grid.darkness.blit( grid.darksquares[square[1]], (x,y) )
						else:
							del grid.darksquares[square[1]]
							grid.darkness.fill(transparent, square[0])
		pygame.event.clear()
	pygame.display.flip()
