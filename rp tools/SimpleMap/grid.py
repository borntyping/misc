import pygame.draw
from collections import defaultdict

def grey(num): return (num,num,num)
black = (0,0,0)
white = (255, 255, 255)
transparent = white

class grid:
	def __init__(self,spacing,size):
		self.spacing = self.s = spacing
		self.xy = self.x, self.y = size
		self.size = self.width, self.height = (self.x * self.spacing), (self.y * self.spacing)
		#self.origin = self.ox, self.oy = origin
		self.squares = list()
		self.darksquares = dict()
		self.tokensquares = dict()
		self.pretty = False
		
	def draw(self,screen):
		self.surface = pygame.Surface(self.size)
		self.surface.convert()
		self.surface.set_colorkey(transparent)
		self.surface.fill(black)
		self.rect = self.surface.get_rect()
		
		self.squares = list()
		for x in range(self.x):
			for y in range(self.y):
				rectx = x * self.s
				recty = y * self.s
				rect  = ((rectx, recty), (self.s,self.s))
				if self.pretty:
					color = (int(x*(255/self.width)), int(y*(255/self.height)), 255)
				else:
					color = transparent
				self.squares.append( (pygame.draw.rect(self.surface, color, rect) , (x,y)) )
		return self.surface
	
	def draw_tokens(self,screen):
		self.tokens = pygame.Surface(self.size)
		self.tokens.convert()
		self.tokens.set_colorkey(transparent)
		self.tokens.fill(transparent)
		return self.tokens
	
	def draw_darkness(self,screen):
		self.darkness = pygame.Surface(self.size)
		self.darkness.convert()
		self.darkness.set_colorkey(transparent)
		self.darkness.fill(transparent)
		return self.darkness
