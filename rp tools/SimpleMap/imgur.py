#!/usr/bin/python

import pycurl, json, StringIO, sys

if len(sys.argv) > 1:
	image = sys.argv[1]
else:
	image = "simplemap.png"
	 
print 'Sending image... ',
c = pycurl.Curl()
values = [
	("key", "f09cf29f098dd6d23a75a8aad9d1ff02"),
	("image", (c.FORM_FILE, image))
]
c.setopt(c.URL, "http://imgur.com/api/upload.json")
c.setopt(c.HTTPPOST, values)
b = StringIO.StringIO()
c.setopt(c.WRITEFUNCTION, b.write)
c.perform()
c.close()
data = b.getvalue()

print "Image sent!"

data = json.loads(data)
original_url = data["rsp"]["image"]["original_image"]
print original_url

import pygtk
pygtk.require('2.0')
import gtk
clipboard = gtk.clipboard_get()
clipboard.set_text(original_url)
clipboard.store()
