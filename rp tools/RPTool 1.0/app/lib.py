from text import *
from config import debugstatus, actions
from manager import listalltools
import sys

def debug(message):
	if (debugstatus == True):
		print "DEBUG: " + message

def isaction(arg,action):
	# If arg is int, check the sys.argv
	# If arg is str, check the string
	# If action is arg, check that the sys.argv exists

	if (action=="arg"):
		if (len(sys.argv) > arg):
			return True
	elif (int(arg)==arg):
		try:
			arg = sys.argv[arg]
		except IndexError:
			print "Stop checking non-existent arguments!"
			
	if (action=='help'):
		if (arg in actions.help):
			return True
	elif (action=='quit'):
		if (arg in actions.quit):
			return True
	return False

def isarg(i):
	if (len(sys.argv) >= i):
		return True
	else:
		return False

def ishelp(i):
	if (len(sys.argv) > i):
		values = ['--help','-h','?']
		if (sys.argv[i] in values):
			del values
			return True
		del values
	return False
	
def isquit(i,check=None):
	if (check == None) or (check == "args"):
		if (len(sys.argv) >= i): quitstr = sys.argv[i]
		else: quitstr = None
	elif (check == "str"): quitstr = i
	else: quitstr = None

	if (quitstr == '-q') or (quitstr == '--quit') or (quitstr == 'quit'):
		return True
	return False

def help():
	print bold + "RPTool" + normal+ " by Samuel Clements"
	print "Run \'rptool [module]\' to activate that module"
	listalltools()

