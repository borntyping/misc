import os, sys

def picktool():
	print "Run tool:",
	activatetool(raw_input())

def activatetool(tool):
	if (tool == "quit") or (tool == "q"):
		quit()
	try:
		exec("import tools." + tool)
	except ImportError:
		print "No such command."

def listalltools():
	print "Tools availible:",
	toollist = os.listdir(sys.path[0] + "/tools")
	toollist.sort()

	newtoollist = list()
	for tool in toollist :
		tool = tool.rsplit('.',1)
		if (len(tool) > 1):
			if (tool[1] == "pyc") or (tool[0] == "__init__"):
				continue
		newtoollist.append(tool[0])
	
	toollist = newtoollist
	del newtoollist, tool

	print "(" + str(len(toollist)) + " tools)",
	
	for i in range(len(toollist)):
		if (i == len(toollist) - 1):
			print toollist[i]
		else:
			print toollist[i] + ",",
