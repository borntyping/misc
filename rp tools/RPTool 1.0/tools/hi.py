import sys
from app.text import bold, normal
from app.lib import ishelp

if (ishelp(2)):
	print bold + "Hello world " + normal + "module."
	print "Prints 'Hello world'"
	quit()

print 'Hello world'
