import sys
from app.text import bold, normal
from app.lib import ishelp
from app.config import appname

def helpmsg():
	print bold + "Dice roller " + normal + "module."

	print "Roll dice in the format XdY+Z, where:"
	print " X is the number of dice,"
	print " Y is the number of sides on the dice"
	print " Z is the number to modify the result by."

	print "Run as '" + appname + " roll skillcheck X Y'",
	print "to check the result of a GURPS style skillcheck, where:"
	print " X is the number rolled, and"
	print " Y is the skill level."
	print "Both values should be between 3 and 18 (3d6)."

if (len(sys.argv) <= 2) or (ishelp(2)):
	helpmsg()
elif (len(sys.argv) > 2):
	if (sys.argv[2] == 'skillcheck') or (sys.argv[2] == 'sc'):
		if (len(sys.argv) < 5):
			print "Too few arguments. Run '" + appname,
			print "roll --help' for syntax"
		else:
			print "Skill check: rolled " + sys.argv[3] + " against " + sys.argv[4]
	else:
		print "Rolling: " + sys.argv[2]
