#if (len(sys.argv) < 5):
#	print "Too few arguments. Run '" + appname,
#	print "roll --help' for syntax"
#else:
#	print "Skill check: rolled " + sys.argv[3] + " against " + sys.argv[4]
#	#from skillcheck import roll
#	#roll(sys.argv[3],sys.argv[4])

import app.text as text
import dice

def roll(skill,diceroll=None):
	if (diceroll in [None,'.']):
		diceroll = dice.roll('3d6','list')
	diceroll = int(diceroll[0])
	skill = int(skill)
	successby = skill - diceroll
	
	print "Rolled " + str(diceroll) + " against a skill of " + str(skill) + ", succeding by " + str(successby) + ".",
	
	if (diceroll > 18 or diceroll < 3):
		print "Sorry, you require addtional 3d6"
	elif (diceroll == 18) or (diceroll == 17 and skill <= 15) or (diceroll >= (skill + 10)):
		print text.bold + text.red + "Critical Failure! [" + str(successby) + "]" + text.normal
	elif (diceroll <= 4) or (diceroll == 5 and skill >= 15) or (diceroll == 6 and skill >= 16):
		print text.bold + text.green + "Critical Success! [+" + str(successby) + "]" + text.normal
	elif (diceroll > skill):
		print text.bold + text.red + "Failure! [" + str(successby) + "]" + text.normal
	elif (diceroll == skill):
		print text.bold + text.green + "Success! [" + str(successby) + "]" + text.normal
	elif (diceroll < skill):
		print text.bold + text.green + "Success! [+" + str(successby) + "]" + text.normal
	return successby
