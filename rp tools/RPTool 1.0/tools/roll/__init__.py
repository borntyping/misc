#
# Python dice roller module for RPTool
#

import sys
from app.text import bold, normal
from app.lib import isaction
from app.config import appname

def helpmsg():
	print bold + "Dice roller " + normal + "module."

	print "Roll dice in the format XdY+Z, where:"
	print " X is the number of dice,"
	print " Y is the number of sides on the dice"
	print " Z is the number to modify the result by."

	print "Run as '" + appname + " roll skillcheck X Y'",
	print "to check the result of a GURPS style skillcheck, where:"
	print " X is the number rolled, and"
	print " Y is the skill level."
	print "Both values should be between 3 and 18 (3d6)."

import dice

if (len(sys.argv) <= 2):
	dice.roll(None,'str')
elif (isaction(2,'help')):
	helpmsg()
else:
	if (sys.argv[2] in ['skillcheck','sc']):
		import skillcheck
		skillcheck.roll(sys.argv[3],sys.argv[4])
	else:
		dice.roll(sys.argv[2],'str')
