from random import randint
import app.config as cfg
from app.lib import debug

# Roll dice in the format XdY+Z, where:
#  X is the number of dice
#  Y is the number of sides on the dice
#  Z is the number to modify the result by

def roll(dicestr=None,returnas=None):
	if (dicestr == '.') or (dicestr == None):
		dicestr = 'd'
	
	if (dicestr[-1] == 'd'):
		dicestr = dicestr + str(cfg.dicesides)
	if (dicestr[0] == 'd'):
		dicestr = str(cfg.dicenum) + dicestr
		
	# Split into list at 'd'
	dieroll = dicestr.split("d")
	numdice = int(dieroll[0])
	dicesides = int(dieroll[1])
	
	i, total  = 0, 0
	result = list()
	
	while (i < numdice):
		i = i + 1
		dieroll = randint(1,dicesides)
		result.append(dieroll)

	total = sum(result)

	if   (returnas == 'list'):
		return (total, result, dicestr)
	elif (returnas == 'string') or (returnas == 'str') or (returnas == None):
		string = "Rolling " + str(dicestr) + "... rolled "+str(total)
		if (numdice > 1): string += ' ' + str(result)
		print string
		return string
