#
# Python ammo module for RPTool
#

import sys
from app.text import bold, normal
from app.lib import isaction
from app.config import appname

def helpmsg():
	print bold + "GURPS Ammo modifier " + normal + "module."
	print "Run as 'ammo modtype damage'"
	
# Functions

def dicemultiply(dicestr,multiplier):
	dice = dicestr.split('d',1)
	if (dice[1] == '') or (dice[1][-1] == 'd'):
		dice[1] += "+0"
	
	multiplier = float(multiplier)
	dice[0] = float(dice[0])
	dice[1] = float(dice[1])

	dice[0] = dice[0] * 3.5
	dice = dice[0] + dice[1]
	
	dice = dice * multiplier
	dice = dice / 3.5
	dice = round(dice,2)
	
	if (dice <= 1.0):
		if   (dice <= 0.32):
			dice = '1d-5'
		elif (dice <= 0.42):
			dice = '1d-4'
		elif (dice <= 0.56):
			dice = '1d-3'
		elif (dice <= 0.75):
			dice = '1d-2'
		elif (dice <= 0.95):
			dice = '1d-1'
		elif (dice >= 0.96):
			dice = '1d'
		else:
			"What."
	else:
		dicefull = int(dice)
		dicefraction = dice % 1
		if   (dicefraction <= 0.15):
			dice = str(dicefull) + 'd'
		elif (dicefraction <= 0.42):
			dice = str(dicefull) + 'd+1'
		elif (dicefraction <= 0.64):
			dice = str(dicefull) + 'd+2'
		elif (dicefraction <= 0.85):
			dicefull = dicefull + 1
			dice = str(dicefull) + 'd-1'
		elif (dicefraction > 0.85):
			dicefull = dicefull + 1
			dice = str(dicefull) + 'd'
	print dice

# Start

if (len(sys.argv) <= 3) or (isaction(2,'help')):
	helpmsg()
else:
	if (sys.argv[2] in ['+P','extrapowerful']):
		print "Modding to extra powerful..."
		multiplier = 1.1
	else:
		multiplier = sys.argv[2]
		
	dicemultiply(sys.argv[3],multiplier)
