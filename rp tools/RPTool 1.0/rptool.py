#!/usr/bin/python

import sys, os
from app.manager import activatetool, listalltools, picktool
from app.lib import isaction, ishelp, help, debug

# Don't write .pyc files (it annoys me)
sys.dont_write_bytecode = True

if (len(sys.argv) > 1):
	if (isaction(1,'help')):
		help()
	else:
		debug(str(sys.argv[1]) + ' args')
		activatetool(sys.argv[1]) # Run tool
else:
	# Show menu
	listalltools()
	picktool()
