bold      ="\x1B[1m"
new       ="\x1B[2m"

black     ="\x1B[30m"
red       ="\x1B[31m"
green     ="\x1B[32m"
yellow    ="\x1B[33m"
blue      ="\x1B[34m"
purple    ="\x1B[35m"
lightblue ="\x1B[36m"
white     ="\x1B[37m"

normal    ="\x1B[m"
