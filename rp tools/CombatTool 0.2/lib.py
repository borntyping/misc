import colors as c
from random import randint

def menutext (menu = "Tools", submenu = ""):
	print c.bold + c.blue + menu + c.normal + ":" + c.lightblue + submenu + c.red + "$" + c.normal,

def box(msg="Put text here!",maincolor=c.red,textcolor=c.blue):
	ln = len(msg)
	print maincolor + u'\u250F' + u'\u2501' * ln + u'\u2513'
	print u'\u2503' + textcolor + c.bold + msg + c.normal + maincolor + u'\u2503'
	print u'\u2517' + u'\u2501' * ln + u'\u251B' + c.normal
	return ln + 2
	
def argsplit(option):
	if (option.find(" ") == -1) :
		return (option,"")
	else :
		option = option.split(" ", 1)
		return (option[0],option[1])
		
def droll(roll):
	dieroll   = roll.split("d")
	dicenum   = int(dieroll[0])
	dicesides = int(dieroll[1])
	i, total  = 0, 0
	rolls = "{"
	while (i < dicenum):
		dieroll = randint(1,dicesides)
		i = i + 1
		rolls += str(dieroll) + ","
		total += dieroll
	rolls += "}"
	# print "Roll: " + roll + " => " + str(total) + " " + rolls
	return (total, rolls, roll)
			
def gurps_roll(skill,roll=None): # skill 14, roll 15
	roll = int(roll)
	skill = int(skill)
	if (roll == None):
		roll = droll("3d6")[0]
	successby = skill - roll
	
	print "Rolled " + str(roll) + " against a skill of " + str(skill) + ", succeding by " + str(successby) + ".",
	
	if (roll > 18 or roll < 3):
		print "Sorry, you require addtional 3d6"
	elif (roll == 18) or (roll == 17 and skill <= 15) or (roll >= (skill + 10)):
		print c.bold + c.red + "Critical Failure! [" + str(successby) + "]" + c.normal
	elif (roll <= 4) or (roll == 5 and skill >= 15) or (roll == 6 and skill >= 16):
		print c.bold + c.green + "Critical Success! [+" + str(successby) + "]" + c.normal
	elif (roll > skill):
		print c.bold + c.red + "Failure! [" + str(successby) + "]" + c.normal
	elif (roll == skill):
		print c.bold + c.green + "Success! [" + str(successby) + "]" + c.normal
	elif (roll < skill):
		print c.bold + c.green + "Success! [+" + str(successby) + "]" + c.normal
	return successby
