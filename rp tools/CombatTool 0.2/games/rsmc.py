print "Red Sun Mecenary Company"
from lib import *

# Functions

def attack(*args):
	count = len(args)
	if (count == 0): raise NameError('Your doing it wrong!')
	if   args[0] in ('shotgun', 'sg', 's'):   shotty(*args[1:])
	elif args[0] in ('gyrocs', 'gyroc', 'g'): gyrocs(*args[1:])

def shotty(*args):
	count = len(args)
	print "Attacking with shotgun"
	if (count < 1):
		print "Enter skill:",
		skill = raw_input()
	else:
		skill = args[0]
	if (count < 2):
		print "Enter roll:",
		roll = raw_input()
	else:
		roll = args[1]
	gurps_roll(skill,roll)
