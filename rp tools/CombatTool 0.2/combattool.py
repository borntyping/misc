#!/usr/bin/python

##############
# CombatTool #
##############

# Imports
from lib import *
import sys

# Init
exitprog, exitask, exitsub = False, False, False
menu = "CombatTool"
game = None

# Command args
count = len(sys.argv)
if (count > 1):
	if (sys.argv[1] == "game"):
		game = sys.argv[2]
	elif (sys.argv[1] == "roll"):
		if (count > 3):
			gurps_roll(int(sys.argv[2]),int(sys.argv[3]))
		else:
			gurps_roll(int(sys.argv[2]))
		quit()

ln = box(menu)
ln = str(ln)

while not (exitask == True):
	if (game == None):
		print "Select game:",
		game = raw_input()
		game = game.lower()

	if   game in ('rsmc', 'rs', 'redsun'): game = "rsmc"
	elif game in ('df'): game = "df"
	elif game in ('quit', 'q'):
		print "\n\"Leaving so soon?\""
		quit()

	try:
		# __import__("games",fromlist=['*'])
		# __import__(game,fromlist=['attack'])
		_temp = __import__("games." + game, globals(), locals(), ['attack'])
		attack = _temp.attack
	except ImportError:
		print "Game not found.",
		game = None
	else:
		exitask = True
		
dir()

while not (exitprog == True):
	menutext(menu,game)
	usrinput = raw_input()
	option, args = argsplit(usrinput)
	if option in ('attack', 'a'):
		while not (exitsub == True):
			menutext(menu,game + "/attack")
			usrinput = raw_input()
			option, args = argsplit(usrinput)
			if option in ('menu', 'm'):
				exitsub = True
			elif option in ('quit', 'q'):
				quit()
			else:
				arg = usrinput.split(" ")
				attack(*arg)
		exitsub = False
	elif option in ('quit', 'q'):
		quit()
