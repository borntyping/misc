#!/usr/bin/php
<?php

// Setings

$padding = 20;
$phrases = 6;
$width = $padding * $phrases;
$sleep = FALSE;

// Init vars

$verbs = array(
	"Communicate", "Heal", "Sense", "Weaken", "Strengthen",
	"Move", "Protect", "Create", "Control" /*, "Transform" */
);

$nouns = array(
	"Air", "Animal", "Body", "Death", "Earth", "Fire", "Food", "Image",
	"Light", "Magic", "Mind", "Plant", "Sound", "Water"
);

if (!isset($sleep) || $sleep == TRUE)
{
	$sleep = 50000;
}

// Intro
echo "Listing all known Syntactic Magic spells";
for ($i = 1; $i <= 3; $i++) {
	if ($sleep != FALSE)
	{
		usleep(500000);
	}
	echo ".";
}
echo "\n".str_pad("",$width,"-")."\n";
echo "\n";

$spells = 0;
$i = 0;

// Two word spells

foreach ($verbs as $verb)
{
	foreach ($nouns as $noun)
	{
		echo str_pad($verb.' '.$noun,$padding);
		if ($sleep != FALSE)
		{
			usleep($sleep);
		}
		$i++; $spells++;
		if ($i >= $phrases)
		{
			echo "\n";
			$i = 0;
		}
	}
	$i = 0;
	echo "\n".str_pad("",$width,"-")."\n";
}

$i = 0;

$padding = 31;
$phrases = 4;

foreach ($nouns as $noun_one)
{
	foreach ($nouns as $noun_two)
	{
		if ($noun_one == $noun_two)
		{
			continue;
		}
		echo str_pad('Transform '.$noun_one.' to '.$noun_two,$padding);
		if ($sleep != FALSE)
		{
			usleep($sleep);
		}
		$i++;
		$spells++;
		if ($i >= $phrases) {
			echo "\n";
			$i = 0;
		}
	}
	$i = 0;
	echo "\n".str_pad("",$width,"-")."\n";
}

echo "$spells spells in total.\n";

?>
