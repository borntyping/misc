defaults = {
	'char_filled' :'+',
	'char_empty' :'-',
	'format' :'%s: %s [%s] %s',
	'min'	 :0,
	'max'	 :10,
	'step'	 :1,

}

class stat:
	def __init__(self, **kwargs):
		self.name		= kwargs.get('name')
		self.minimum	= kwargs.get('minimum', defaults['min'])
		self.maximum	= kwargs.get('maximum', defaults['max'])
		self.value		= kwargs.get('value', self.maximum)
		self.step		= kwargs.get('step', defaults['step'])

	def plus(self, num = 1):
		if self.value < self.maximum:
			self.value += num

	def minus(self, num = 1):
		if self.value > 0:
			self.value -= num

class Bar(stat):
	def __init__(self, **kwargs):
		self.char		= kwargs.get('char_filled', defaults['char_filled'])
		self.char_empty = kwargs.get('char_empty', defaults['char_empty'])
		self.format		= kwargs.get('format', defaults['format'])

	def echo(self):
		value = str(self.char) * self.value
		value = value.ljust(self.maximum, self.char_empty)
		bar = self.format % (self.name.ljust(10), self.minimum, value, self.maximum)
		return bar
