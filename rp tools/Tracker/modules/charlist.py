from character import Character
import curses

class Charlist:
	def __init__(self):
		self.chars = list()

	def __getitem__(self, key):
		return self.chars[key]

	def __setitem__(self, key, value):
		self.chars[key] = value
		return value

	def __str__(self):
		return str(self.chars)

	def newcharacter(self, name, bars):
		print "Creating new character: ", name,
		print bars
		self.chars.append(Character(name,bars))
