#!/usr/bin/env python
from modules.charlist import Charlist
import curses

def main(stdscr):
	# Curses
	height, width = stdscr.getmaxyx()
	stdscr.border(0)

	stdscr.refresh()
	# Character list
	charlist = Charlist()
	charlist.newcharacter('Sam', [{'name': 'Hitpoints'}, {'name': 'Mana'}])
	charlist.newcharacter('Jack', [{'name': 'Hitpoints'}, {'name': 'Mana'}])

	# Print charlist

	pad = curses.newpad(height-5,width-4)
	pad.addstr(0, 0, "Character list:")
	pad.addstr(1, 0, str(charlist.chars))
	line = 1
	chars = ["Sam", "Joe"]
	for c in chars:
		line += 1
		pad.addstr(line, 2, c)
	pad.refresh(0,0,2,2,height-4,width-2)

	stdscr.refresh()

	#newpad = curses.newpad(10,width - 2)
	#newpad.addstr(1, 1, "Some text")
	#newpad.refresh(0,0, 5,1, 15,width-2)

	stdscr.refresh()
	stdscr.getch()

if __name__ == '__main__':
	curses.wrapper(main)
