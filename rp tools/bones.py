#!/usr/bin/python

import random

class Die:
	def __init__(self,sides):
		self.sides = sides
		self.facet = False

	def __str__(self):
		text = 'd' + str(self.sides)
		if self.facet != False: text += ' (' + str(self.facet) + ')'
		return text

	def roll(self):
		self.facet = random.randint(1,self.sides)
		return self.facet

	def clear(self):
		self.facet = False

class Dice:
	def __init__(self,dice):
		dice = parse(dice)
		number = int(dice['number'])
		sides = int(dice['sides'])
		self.mod = int(dice['mod'])
		self.dice = []
		for i in range(number):
			self.dice.append( Die(sides) )

	def __str__(self):
		if self.dice:
			a = []
			for die in self.dice:
				a.append(str(die))
			return "A bag of dice containing: " + ', '.join(a) + "."
		else:
			return "An empty bag of dice."

	def adddie(self,die):
		self.dice.append(die)

	def adddice(self,Dice):
		""" Merges two dicebags. """
		self.mod += Dice.mod
		for die in Dice.dice:
			self.adddie(die)
		return self

	def roll(self):
		for die in self.dice:
			die.roll()
		return self

	def clear(self):
		for die in self.dice:
			die.clear()
		return self

	def peek(self):
		a = []
		total = 0
		for die in self.dice:
			a.append(str(die.facet))
			total += die.facet
		total += self.mod
		return (a, total)

	def look(self):
		a, total = self.peek()
		text = "The dice show " + ', '.join(a) + ", totaling " + str(total) + "."
		if self.mod < 0:
			text += " [-" + str(self.mod) + "]"
		elif self.mod > 0:
			text += " [+" + str(self.mod) + "]"
		return text

def parse(string):
	if 'd' in string:
		number, sides = string.split('d')
	else:
		number, sides = 1, string

	if '+' in sides:
		sides, mod = sides.split('+')
		mod = int(mod)
	elif '-' in sides:
		sides, mod = sides.split('-')
		mod = -1*int(mod)
	else:
		mod = 0

	number = int(number)
	sides = int(sides)

	return {'number': number, 'sides': sides, 'mod': mod}

if __name__ == '__main__':
	print "Dicebag example"
	x = Dice('3d6+1')
	print x
	print x.roll()
	print x.look()
	print x.clear()