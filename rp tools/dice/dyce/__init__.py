# -*- coding: utf-8 -*-
"""dyce -- General purpose python dice toolkit and custom expression parser.

$Author: david.eyk $\n
$Rev: 9 $\n
$Date: 2008-12-09 19:39:47 -0600 (Tue, 09 Dec 2008) $
"""

__author__ = "$Author: david.eyk $"[9:-2]
__version__ = "$Rev: 9 $"[6:-2]
__date__ = "$Date: 2008-12-09 19:39:47 -0600 (Tue, 09 Dec 2008) $"[7:-2]

from dice import *
#from dcalc import *

roller = Dice()

