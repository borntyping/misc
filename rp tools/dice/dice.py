#!/usr/bin/python

from dyce.dice import parse, Dice

dice = Dice()
roll = parse('3d6+5')

dice._cheat_next = [
	[6,6,6],
	[5,5,5],
	[4,4,4]
]

cheat = '[3,3,3]'
dice._cheat_next.append(eval(cheat))

for x in range(0):
	rolled, total = dice.rollsum(roll[0],roll[1],0,roll[2])
	print str(rolled),
	print total

for x in range(10):
	print dice.rollbell(1,6)
