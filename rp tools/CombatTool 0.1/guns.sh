weapon_gyrocs(){
	until [ "$endfunction" = 1 ]; do
		echo -e "\033[1m\E[34mAttacking with gyrocs:\033[0m"; tput sgr0
		echo    "Roll vs. Artillery (Guided Missile)"
		echo -n "Enter roll vs skill: "
		if [ -z $1 ]; then
			read input
		else
			input=$1
		fi
		sroll $input
		if [ $success = 1 ] ; then
			echo -n "Aim successful. "
			skill=14
		else
			echo -n "Aim unsuccessful. "
			skill=13
		fi
		echo "Gyroc attacks with skill $skill and 6d p++ damage." #CHECK
		echo -n "Type q, or roll again: "
		read do
		if [ "$do" = "q" ]; then
			endfunction=1
		else
			weapon_gyrocs $do
		fi
	done
}
