#!/bin/bash

source ./lib.sh
source ./guns/gyroc.sh

clear
echo -e "\E[34m┏━━━━━━━━━━━━━━━━━━━━━┓"
echo -e "\E[34m┃\033[1m\E[31mWelcome to CombatTool\033[0m\E[34m┃"
echo -e "\E[34m┗━━━━━━━━━━━━━━━━━━━━━┛"; tput sgr0

# Main menu
until [ "$exitall" = 1 ]; do
	menuoption=""
	# Read or get menuoption
	echo -en "\033[1m\E[33mSelect weapon:\E[31m$ "; tput sgr0
	if [ -z $1 ] || [ "$skipoption" = 1 ]; then
		read menuoption
	else
		echo $*
		menuoption=$*
		skipoption=1
	fi
	menu(){
		chkexit $1
		case "$1" in
			gyroc|gyrocs)
				weapon_gyrocs $2 $3 $4
				;;
			roll|sroll|r)
				sroll $2 $3
				;;
			shotgun)
				weapon_shotgun
				;;
			*)
				echo try again
				;;
		esac
	}
	menu ${menuoption}
done
