act="Type q for menu, press enter or roll again to continue using this weapon: "
helpmsgs="--help -h ? help"

credits(){
	echo -e "\E[1m\E[34mCombatTool - (c) Samuel Clements 2010\033[0m"
	echo -e "\E[4mThanks for using this tool!\033[0m"
	sleep 1
	clear
	exit
}

chkexit(){
	if [ "$1" == "q" ]; then
		endfunction=1
		continue
	elif [ "$1" == "exit" ]; then
		credits
	fi
}

sroll(){
	if [ -z $1 ] || [ -z $2 ]; then
		echo "sroll() is missing parameters"
	fi
	r=$1
	s=$2
  successby=$(($s-$r))
  echo -n "You rolled $r against a skill of $s. "
  if [ $r -ge 19 ] || [ $r -le 2 ]; then
  	echo "\"Check dice for damage\""
	elif [ $r -eq 18 ] || ( [ $r -eq 17 ] && [ $s -le 15 ] ) || [ $r -ge $(($s + 10)) ]; then
  	echo -e "\033[1m\033[4m\E[31mCritical Failure! [-$successby]\033[0m"; tput sgr0
  	success=0
  elif   [ $r -le  4 ] || ( [ $r -eq  5 ] && [ $s -ge 15 ] ) || ( [ $r -eq 6 ] && [ $s -ge 16 ] )  ; then
 		echo -e "\033[1m\033[4m\E[32mCritical Success! [++$successby]\033[0m"; tput sgr0
		success=1
	elif [ $r -gt $s ]; then
  	echo -e "\033[1m\E[31mFailure [$successby]\033[0m"; tput sgr0
  	success=0
  elif [ $r -eq $s ]; then
  	echo -e "\033[1m\E[32mSuccess [$successby]\033[0m"; tput sgr0
  	success=1
  elif [ $r -lt $s ]; then
  	echo -e "\033[1m\E[32mSuccess! [+$successby]\033[0m"; tput sgr0
  	success=1
  fi
}
