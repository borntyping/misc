weapon_gyrocs(){
	endfunction=0	
	until [ "$endfunction" = 1 ]; do
		if [ "$1" = "help" ]; then
			echo "Usage: gyrocs [AIM ROLL] [SKILL] [ATTACK ROLL]"
			echo "Usage: gyrocs -h       Summons this message"
			echo "              --help"
			echo "If no parameters are given, gyrocs will ask for each roll or number as needed."
			chkexit "q"
		fi
		
		echo -e "\E[34mAttacking with gyrocs:"; tput sgr0		
		# Read or get roll
		echo "Aim manouver: roll vs. Artillery (Guided Missile) [$2]"
		if [ -z $1 ]; then
			echo "Enter roll and skill: "
			read input
			chkexit $input
			roll=(`echo $input | cut -d ' ' -f 1`)
			skill=(`echo $input | cut -d ' ' -f 2`)
			if [ "$skill" = "" ]; then
				echo -n "Enter skill: "
				read input
				chkexit $input
				skill=$input
			fi
		elif [ -z $2 ]; then
			echo -n "Enter skill: "
			read input
			chkexit $input
			roll=$1
			skill=$input
		else
			input=$*
		fi
		sroll $roll $skill
		
		# Echo 'lock on' success
		echo -n "1 : Aim   : "
		if [ "$success" = 1 ] ; then
			echo "Gyroc locked on. "
			aimskill=14
		else
			echo "Gyroc has not locked on. "
			aimskill=13
		fi
		
		echo "Ready manouver: roll vs. Gyroc's Skill [$aimskill]"
		if [ -z $3 ]; then
			echo -n "Enter roll: "
			read input
			chkexit $input
			input="$input $aimskill"
		else
			input="$3 $aimskill"
		fi
		sroll ${input}	
		
		echo "2 : Ready : Gyroc attacks with 6d p++ damage and ROF3." #CHECK
		echo -n $act
		read do
		chkexit $do
		weapon_gyrocs $do
	done
}
