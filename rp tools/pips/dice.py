import random

class Dice ():
	def __init__ (self, sides):
		self.sides = sides

	def roll (self, n = 1):
		results = []
		for i in xrange(0, n):
			results.append(self._roll())
		return results
	
	def _roll (self):
		return random.randint( 1, self.sides )
	
	def __str__ (self):
		return "<%s sided dice>" % ( self.sides )

class HistoryDice (Dice):
	def __init__ (self, sides, len = 10):
		Dice.__init__(self, sides)
		self.len = len
		self.history = []
	
	def _roll(self):
		roll = Dice._roll(self)
		self.history.append(roll)
		if len(self.history) > self.len:
			self.history = self.history[-self.len:]
		return roll
	
	def __str__ (self):
		return "<%s sided dice, history of %s rolls>" % ( self.sides, self.len )
