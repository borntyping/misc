class Counter ():
	def __init__ (self, **arguments):
		self.min = arguments.get('min', 0)
		self.max = arguments.get('max', 10)
		self.i	 = arguments.get('start', 0)
		
	def __str__ (self):
		return "<Counter at %s {%s->%s}>" % (self.i, self.min, self.max)
	
	def edit(self, i):
		self.i = self.i + i
		if self.i < self.min:
			remaining = self.min - self.i
			self.i = self.min
		elif self.i > self.max:
			remaining = self.i - self.max
			self.i = self.max
		else:
			remaining = 0
		return {'i': self.i, 'remaining': remaining}
	
	def up (self, i = 1):
		return self.edit(i)
	
	def down (self, i):
		return self.edit(-i)
