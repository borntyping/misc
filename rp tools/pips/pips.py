#!/usr/bin/env python
"""

Pips - Dice toolkit

Samuel Clements [ziaix] (c) 2010

"""

from dice import *
from counters import *

def test_dice():
	d = Dice(6)
	print d
	print d.roll(5)
	
def test_history_dice():	
	h = HistoryDice(6,2)
	print h
	print h.roll(5)
	print h.history

def test_counters():
	c = Counter()
	print c
	print c.up(2)
	print c.down(4)
	print c.up(12)

if __name__ == '__main__':
	test_dice()
	print
	test_history_dice()
	print
	test_counters()
