class cell:
	def __init__(self, pos, shape, linewidth = 1):
		self.pos = pos
		self.shape = shape
		self.linewidth = linewidth

	def get_vertices(self):
		self.shape.calc_vertices()
		self.vertices = []
		x, y = self.shape.xy(self.pos)
		for v in range(len(self.shape.vertices)):
			i, j = self.shape.vertices[v]
			self.vertices.append((x + i, y + j))
		return self.vertices

	def set_linewidth(self, linewidth):
		self.linewidth = linewidth

	def __str__(self):
		return __name__ + " instance at " + str(self.pos)