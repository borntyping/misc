from math import sqrt

class shape:
	def __init__(self, height, width):
		self.height, self.width = width, height
		self.calc_vertices()
	def xy(self, pos):
		return int( pos[0] * self.width ), int ( pos[1] * self.height )
	def calc_vertices(self):
		self.vertices = [(0,0), (self.width,0), (self.width,self.height), (0,self.height)]

class square(shape):
	def __init__(self, side):
		self.side = side
		shape.__init__(self,side,side)

class rectangle(shape):
	pass

class circle(shape):
	def __init__(self, radius):
		self.radius = radius
		self.diameter = radius * 2
		shape.__init__(self, self.diameter, self.diameter)

	def xy(self, pos):
		x = int( pos[0] * self.width + self.radius )
		y = int ( pos[1] * self.height + self.radius )
		return (x,y)

class hexagon(shape):
	def __init__(self, radius):
		self.radius = radius
		self.side = int(round(1.5 * self.radius))
		self.width = int(round(2.0 * self.radius))
		self.height = int(round(sqrt(3.0) * self.radius))
		self.calc_vertices()

	def calc_vertices(self):
		""" Get a list of vertices that can be used to draw a line """
		self.vertices = []
		self.vertices.append( (self.side - self.radius, 0) )
		self.vertices.append( (self.side, 0) )
		self.vertices.append( (self.width, self.height / 2) )
		self.vertices.append( (self.side, self.height) )
		self.vertices.append( (self.side - self.radius, self.height) )
		self.vertices.append( (0, self.height / 2) )
		return self.vertices

	def xy(self, pos):
		return (pos[0] * self.side , pos[1] * self.height + pos[0]%2 * (self.height / 2))