#!/usr/bin/python

# 'Hex' - Hexmapper tool
# (c) Samuel 'Ziaix' Clements 2011

"""
Todo:
	Click detection
	Toggle onclick

	hex.hex stores tokens

	options/settings panel
"""

# Imports
import pygame
import sys

import shape
from grid import *

# Pygame startup
pygame.init()
pygame.display.set_caption('Hex')

# Set display
size = width, height = (601,301)
screen = pygame.display.set_mode(size,pygame.RESIZABLE)
pygame.event.post( pygame.event.Event(pygame.VIDEORESIZE, {'size': size}))

while exit != True:
	event = pygame.event.wait()
	if (event.type == pygame.QUIT) or ((event.type == pygame.KEYDOWN) and (event.key == pygame.K_q)):
		exit = True
	elif event.type == pygame.VIDEORESIZE:
		screen = pygame.display.set_mode(event.size,pygame.RESIZABLE)
		size = width, height = event.size
	elif event.type == pygame.MOUSEBUTTONDOWN:
		pass
	else:
		continue

	# Load background
	size = width, height = screen.get_size()
	background = pygame.Surface(size)
	background = background.convert()
	background.fill((0,0,50))

	mygrid = newgrid(shape.circle(3), (width - 20, height - 20))

	screen.blit(background, (0,0))
	screen.blit(mygrid.surface, (10,10))
	pygame.display.flip()
sys.exit()