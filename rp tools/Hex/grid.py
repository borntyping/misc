import pygame
import shape
from cell import cell
from math import floor

class GridError(Exception):
	pass

def newgrid(newshape, size):
	if isinstance(newshape, shape.hexagon):
		return hexgrid(newshape, size)
	elif isinstance(newshape, shape.circle):
		return circlegrid(newshape, size)
	elif isinstance(newshape, (shape.square, shape.rectangle)):
		return grid(newshape, size)
	raise GridError("Shape not supported.")

class grid:
	def __init__(self, shape, size, color = (255,255,255), background = (0,0,0)):
		self.size = { 'width': size[0], 'height': size[1] }
		self.surface = pygame.Surface(size)
		self.surface.fill(background)
		self.color = color
		self.cells = dict()
		self.shape = shape
		self.set_gridsize()
		self.create_cells()
		self.draw_cells()

	def init(self, shape):
		self.shape = shape
		self.set_gridsize()
		self.create_cells()
		self.draw_cells()

	def set_gridsize(self):
		self.gridsize = {
			'x': int(floor(self.size['width']/self.shape.width)),
			'y': int(floor(self.size['height']/self.shape.height)),
		}

	def create_cells(self):
		for x in range(self.gridsize['x']):
			for y in range(self.gridsize['y']):
				self.cells[(x,y)] = cell((x,y), self.shape)

	def draw_cells(self, linewidth = False):
		for h in self.cells:
			if linewidth == False:
				linewidth = self.cells[h].linewidth
			pygame.draw.polygon(self.surface, self.color, self.cells[h].get_vertices(), linewidth)

	def __setitem__(self, key, value): self.cells[key] = value
	def __getitem__(self, key): return self.cells[key]

	def __iter__(self): return iter(self.cells)
	def keys(self): return iter(self.cells.keys())

# Gridtypes

class circlegrid(grid):
	def draw_cells(self, linewidth = False):
		for h in self.cells:
			if linewidth == False:
				linewidth = self.cells[h].linewidth
			pygame.draw.circle(self.surface, self.color, self.cells[h].shape.xy(self.cells[h].pos), self.cells[h].shape.radius, linewidth)

class hexgrid(grid):
	def set_gridsize(self):
		self.gridsize = {
			'x': int(floor(self.size['width']/self.shape.side)),
			'y': int(floor(self.size['height']/self.shape.height)),
		}