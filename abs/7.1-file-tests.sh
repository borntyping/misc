#!/usr/bin/env bash

function show_input_type()
{
   [ -p /dev/fd/0 ] && echo "PIPE" || echo "STDIN"
}

echo "Current input type:"
show_input_type

echo

echo "Pipe input type:"
echo "Input" | show_input_type
