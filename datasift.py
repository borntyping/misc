#!/usr/bin/env python

import sys
import argparse
import fileinput
import json

# Parse arguments
parser = argparse.ArgumentParser(description='Reads exported data from DataSift tasks')
parser.add_argument('-i', '--item', type=int, help="the item to show, or to start at if --count is provided")
parser.add_argument('-c', '--count', type=int, help="the number of items to show. defaults to all")
parser.add_argument('files', nargs='*', help="the file(s) to read from")
args = parser.parse_args()

# Setting either -i or -c implicity sets the other option
if args.item and not args.count:
	args.count = 1
elif args.count and not args.item:
	args.item = 0

def proccess_file (filename, refrence=None):
	"""
	Processes a single file, using args to only show the requested items
	
	Uses fileinput to deal with compressed files
	
	:Parameters:
		- filename (str): The name of the file to output
		- refrence: An identifier for the current file
	"""
	for line_number, line in enumerate(fileinput.FileInput(filename, openhook=fileinput.hook_compressed)):
		if not args.item or (line_number >= args.item and line_number < args.item + args.count):
			print ("Item {}" if refrence is None else "Item {} from file {}").format(line_number, refrence)
			iteraction = json.loads(line)
			print json.dumps(iteraction, sort_keys=True, indent=2)
	
def proccess_all_files (files):
	"""
	Processes a list of files, or stdin if no filenames are given
	"""
	if not files:
		files = ['-']
		
	for f, filename in enumerate(files):
		proccess_file(filename, refrence=f)

if __name__ == '__main__':
	proccess_all_files(list(args.files))
