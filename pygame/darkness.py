#!/usr/bin/env python

"""
Darkness, a science fiction rougelike
"""

# Initialise pygame
import pygame
pygame.display.init()
pygame.font.init()

# Initialise the user interface
from visual.main import UserInterface

class Darkness ():
	def __init__ (self, run = False):
		"""	Initialise the game """
		self.ui = UserInterface()

		# Run the game once initialisation is complete
		if run: self.run()

	def run (self):
		"""	Run the event loop """
		while 1:
			for event in pygame.event.get():
				if event.type == pygame.QUIT:
					return
			self.ui.draw()


# Run game
if __name__ == '__main__':
	Darkness(run = True)