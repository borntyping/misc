"""
User interface classes
"""

import pygame

from pygame import Surface

class UserInterface ():
	def __init__ (self):
		"""	Create the user interface """
		# Initialise screen
		pygame.display.set_caption('Darkness')

		self.set_size(700, 600)

		self.screen = pygame.display.set_mode(self.size)

		# Create surfaces
		self.background = Surface(self.screen.get_size()).convert()
		self.playing_surface = PlayingSurface((500, 500))

	def set_size (self, w, h):
		"""	Set the size of the ui """
		self.size = self.width, self.height = w, h

	def draw (self):
		"""	Draw the main ui """
		# Draw playing surface to background
		self.playing_surface.draw(self.background)

		# Blit everything to the screen
		self.screen.blit(self.background, (0, 0))
		pygame.display.flip()

class PlayingSurface (Surface):
	def __init__ (self, size):
		Surface.__init__(self, size)
		self.fill((50,50,50))

	def draw (self, surface):
		""" Draw the surface onto the center of the given surface """
		pos = self.get_rect()
		pos.center = surface.get_rect().center
		surface.blit(self.convert(), pos)